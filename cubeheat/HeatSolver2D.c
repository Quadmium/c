#include "mpi.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Block 1
// Forward declarations
void getLocalSize(MPI_Comm cartComm, int dim, int n, int * nLocal, int * offset);
void setToZero(int xSize, int ySize, double * u);
void getXY(int i, int j, int xOffset, int yOffset,
        double dx, double dy, double * x, double * y);
void setBCs(MPI_Comm cartComm, int xSize, int ySize, int xOffset, int yOffset,
        double dx, double dy, double * u);
double bcFunc(double x, double y);
void ForwardTime(int xSize, int ySize,
        double dx, double dy, double dt, double D,
        const double * u, double * unew);
void setEqual(int xSize, int ySize, const double * u1, double * u2);
void exchangeBoundaryData(MPI_Comm cartComm, int xSize, int ySize, double * u, MPI_Datatype rowType, MPI_Datatype colType);
void outputToFile(MPI_Comm cartComm, double * u, int nx, int ny, int gridSizeX, int gridSizeY,
        int xOffset, int yOffset, double dx, double dy);
void writeToFile(const double * u, int nx, int ny, double dx, double dy);
//--------------------------------------------------------------------------------
// Block 2

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);

    // Erase existing
    remove("Heat2D.dat");

    // Set some parameters
    double Lx = 1.0;
    double Ly = 1.0;
    int nx = 50; // Number of cells in x direction (nnodesx = nx+1)
    int ny = 50; // Number of cells in y direction (nnodesy = ny+1);
    
    // Compute some things
    double dx = Lx / nx;
    double dy = Ly / ny;

    // Determine time step from Diffusivity
    // and grid size
    double D = 0.1;
    
    double dtMax = 1.0 / 2 / (1/(dx*dx) + 1/(dy*dy)) / D; //(1.0 / 6.0)*(dx * dx / D);
    double dt = 0.5 * dtMax;
    double Tmax = 0.5;
    double Numstep = floor(Tmax / dt);
    double Time = 0.0;
    double PercComp = 0.0;

    // Write out details about Time stepping
    //  std::cout << "Max Time Step =  " << dtMax << std::endl;
    //  std::cout << "Using Time Step =  " << dt << std::endl;
    //  std::cout << "Number of Steps =  " << Numstep << std::endl;
    //  std::cout << "Total Sim Time = " << Numstep*dt << std::endl;

    // Get my processor ID and number of processors
    int myProcID, numProcs;
    MPI_Comm_rank(MPI_COMM_WORLD, &myProcID);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);

    // Set up Cartesian topology
    int ndims = 2;
    int dims[2] = {0, 0};
    MPI_Dims_create(numProcs, ndims, dims);
    int periods[2] = {0, 0};
    int reorder = 1;
    MPI_Comm cartComm;
    MPI_Cart_create(MPI_COMM_WORLD, ndims, dims,
            periods, reorder, &cartComm);
    int cartRank;
    MPI_Comm_rank(cartComm, &cartRank);

    // Compute size of my local grid.
    int nxLocal, nyLocal;
    int xOffset, yOffset;
    getLocalSize(cartComm, 0, nx, &nxLocal, &xOffset);
    getLocalSize(cartComm, 1, ny, &nyLocal, &yOffset);

    // Allocate memory for grid -- (nxLocal+2) by (nyLocal+2) to account
    // for boundary or ghost nodes  
    int gridSizeX = nxLocal + 2;
    int gridSizeY = nyLocal + 2;
    double * u = malloc(sizeof (double) * gridSizeX * gridSizeY);
    double * unew = malloc(sizeof (double) * gridSizeX * gridSizeY);

    // Set up initial state: zeros except for boundary condition.
    setToZero(gridSizeX, gridSizeY, u);
    setToZero(gridSizeX, gridSizeY, unew);
    setBCs(cartComm, gridSizeX, gridSizeY, xOffset, yOffset, dx, dy, u);
    setBCs(cartComm, gridSizeX, gridSizeY, xOffset, yOffset, dx, dy, unew);
    
    // Define row and column data types
    MPI_Datatype rowType, colType;
    MPI_Type_contiguous(gridSizeX, MPI_DOUBLE, &rowType);
    MPI_Type_vector(gridSizeY, 1, gridSizeX, MPI_DOUBLE, &colType);
    MPI_Type_commit(&rowType);
    MPI_Type_commit(&colType);
    
    //------------------------------------------------------------------------------  
    // Block 3
    // Begin Time Stepping
    for (int k = 0; k < Numstep; k++) {

        // Forward Euler
        ForwardTime(gridSizeX, gridSizeY, dx, dy, dt, D, u, unew);

        // Exchange process boundary data in unew
        exchangeBoundaryData(cartComm, gridSizeX, gridSizeY, unew, rowType, colType);

        // Set u equal to unew
        setEqual(gridSizeX, gridSizeY, unew, u);

        // Compute Time and Percent Complete
        Time = dt * (k + 1);
        PercComp = 100.0 * (1.0 * k) / Numstep;

        // Master Core Prints out Progress Check
        if ((myProcID == 0) && (k % 50) == 0)
            printf("Progress Check: %f Percent Done!\n", PercComp);

    }
    //---------------------------------------------------------------------------------
    outputToFile(cartComm, u, nx, ny, gridSizeX, gridSizeY, xOffset, yOffset, dx, dy);

    // Clean up
    free(u);
    free(unew);
    
    MPI_Type_free(&rowType);
    MPI_Type_free(&colType);

    MPI_Finalize();
    return 0;
}
//-------------------------------------------------------------------------------------
//Block 5

void getLocalSize(MPI_Comm cartComm, int dim, int n, int * nLocal, int * offset) {
    // Get my process coordinates
    int cartDims[2];
    int periods[2];
    int cartCoords[2];
    MPI_Cart_get(cartComm, 2, cartDims, periods, cartCoords);

    // Compute number of local interior nodes, and offset
    // global_I = local_i + offset
    *nLocal = (n - 1) / cartDims[dim];
    *offset = cartCoords[dim] * *nLocal;

    // Account for non-uniform distribution on last proc: offset + nlocal + 1 = n
    if (cartCoords[dim] == cartDims[dim] - 1) {
        *nLocal = n - 1 - *offset;
    }
}
//---------------------------------------------------------------------------------------
// Block 6

void setToZero(int xSize, int ySize, double * u) {
    memset(u, 0, sizeof(double) * ySize * xSize);
    /*for (int i = 0; i < xSize; ++i) {
        for (int j = 0; j < ySize; ++j) {
            u[ySize * i + j] = 0.0;
        }
    }*/
}
//---------------------------------------------------------------------------------------
// Block 7

void setBCs(MPI_Comm cartComm, int xSize, int ySize, int xOffset, int yOffset,
        double dx, double dy, double * u) {
    // Get my process coordinates
    int cartDims[2];
    int periods[2];
    int cartCoords[2];
    MPI_Cart_get(cartComm, 2, cartDims, periods, cartCoords);

    double x, y;
    int sizeMap[] = {xSize, ySize};
    
    for(int c = 0; c < 2; c++)
    {
        if(cartCoords[c] == 0 || cartCoords[c] == cartDims[c] - 1)
        {
            int j = cartCoords[c] == 0 ? 0 : (sizeMap[c] - 1);
            for(int i=0; i<sizeMap[1-c]; i++)
            {
                int k=i,l=j;
                if(c == 0) {k=j; l=i;}
                getXY(k, l, xOffset, yOffset, dx, dy, &x, &y);
                u[xSize * l + k] = bcFunc(x,y);
            }
        }
    }
    
    /*
    // Lower BC?
    if (cartCoords[1] == 0) {
        int j = 0;
        for (int i = 0; i < xSize; ++i) {
            getXY(i, j, xOffset, yOffset, dx, dy, &x, &y);
            u[xSize * j + i] = bcFunc(x, y);
        }
    }

    // Upper BC?
    if (cartCoords[1] == cartDims[1] - 1) {
        int j = ySize - 1;
        for (int i = 0; i < xSize; ++i) {
            getXY(i, j, xOffset, yOffset, dx, dy, &x, &y);
            u[xSize * j + i] = bcFunc(x, y);
        }
    }

    // Left BC?
    if (cartCoords[0] == 0) {
        int i = 0;
        for (int j = 0; j < ySize; ++j) {
            getXY(i, j, xOffset, yOffset, dx, dy, &x, &y);
            u[xSize * j + i] = bcFunc(x, y);
        }
    }

    // Right BC?
    if (cartCoords[0] == cartDims[0] - 1) {
        int i = xSize - 1;
        for (int j = 0; j < ySize; ++j) {
            getXY(i, j, xOffset, yOffset, dx, dy, &x, &y);
            u[xSize * j + i] = bcFunc(x, y);
        }
    }*/

}
//-----------------------------------------------------------------------------
// Block 8

void getXY(int i, int j, int xOffset, int yOffset,
        double dx, double dy, double * x, double * y) {
    *x = (i + xOffset) * dx;
    *y = (j + yOffset) * dy;
}
//----------------------------------------------------------------------------
// Block 9

double bcFunc(double x, double y) {
    double u = 1.0;
    return u;
}

//--------------------------------------------------------------------------------
// Block 10

void ForwardTime(int xSize, int ySize,
        double dx, double dy, double dt, double D,
        const double * u, double * unew) {
    double dx2 = dx*dx;
    double dy2 = dy*dy;
    
    for (int j = 1; j < ySize - 1; ++j) {
        for (int i = 1; i < xSize - 1; ++i) {
            double uE = u[xSize * (j) + i + 1 ];
            double uW = u[xSize * (j) + i - 1 ];
            double uN = u[xSize * (j + 1) + i ];
            double uS = u[xSize * (j - 1) + i ];
            double uC = u[xSize * (j) + i ];
            double xDer = (uE+uW-2*uC)/dx2;
            double yDer = (uN+uS-2*uC)/dy2;
            
            unew[xSize * j + i] = uC + D*dt*(xDer + yDer);
            
            /*unew[xSize * j + i] = (D * dt) / (dx2 * dy2)*
                    (dy2 * (uE + uW) + dx2 * (uN + uS)
                    - 2.0 * uC * (dx2 + dy2)) + uC;*/
        }
    }
}
//-----------------------------------------------------------------------------------
// Block 11

void setEqual(int xSize, int ySize, const double * u1, double * u2) {
    memcpy(u2, u1, sizeof(double) * xSize * ySize);
    /*
    for (int j = 0; j < ySize; ++j) {
        for (int i = 0; i < xSize; ++i) {
            u2[xSize * j + i] = u1[xSize * j + i];
        }
    }*/
}
//-----------------------------------------------------------------------------------
// Block 12

void exchangeBoundaryData(MPI_Comm cartComm, int xSize, int ySize, double * u, MPI_Datatype rowType, MPI_Datatype colType) {

    // Get the processes at right, left, up and down
    int procR, procL, procU, procD;
    MPI_Cart_shift(cartComm, 0, 1, &procL, &procR);
    MPI_Cart_shift(cartComm, 1, 1, &procD, &procU);

    MPI_Status status;

    // Communicate up (send my top row up, recv my bottom boundary from down)
    MPI_Sendrecv(&u[xSize * (ySize - 2)], 1, rowType, procU, 0,
            &u[0], 1, rowType, procD, 0,
            cartComm, &status);

    // Communicate down (send my bottom row down, recv my top boundary from up)
    MPI_Sendrecv(&u[xSize], 1, rowType, procD, 0,
            &u[xSize * (ySize - 1)], 1, rowType, procU, 0,
            cartComm, &status);

    // Communicate right (send my right col right, recv my left boundary from left)
    MPI_Sendrecv(&u[xSize - 2], 1, colType, procR, 0,
            &u[0], 1, colType, procL, 0,
            cartComm, &status);

    // Communicate left (send my left col left, recv my right boundary from right)
    MPI_Sendrecv(&u[1], 1, colType, procL, 0,
            &u[xSize - 1], 1, colType, procR, 0,
            cartComm, &status);

}
//---------------------------------------------------------------------------------------
// Block 13

void outputToFile(MPI_Comm cartComm, double * u, int nx, int ny, int gridSizeX, int gridSizeY,
        int xOffset, int yOffset, double dx, double dy) {
    int myProcID, numProcs;
    MPI_Comm_rank(cartComm, &myProcID);
    MPI_Comm_size(cartComm, &numProcs);

    // On process 0, allocate data for entire u array, as well as arrays
    // to hold grid sizes and offsets gathered from individual procs
    double * uAll;
    int *gridSizeXArray, *gridSizeYArray, *xOffsetArray, *yOffsetArray;
    int fullSizeX = nx + 1;
    int fullSizeY = ny + 1;
    if (myProcID == 0) {
        uAll = malloc(sizeof (double) * fullSizeX * fullSizeY);
        gridSizeXArray = malloc(sizeof (int) * numProcs);
        gridSizeYArray = malloc(sizeof (int) * numProcs);
        xOffsetArray = malloc(sizeof (int) * numProcs);
        yOffsetArray = malloc(sizeof (int) * numProcs);
    }

    // Gather grid sizes and offsets
    MPI_Gather(&gridSizeX, 1, MPI_INTEGER,
            gridSizeXArray, 1, MPI_INTEGER, 0, cartComm);
    MPI_Gather(&gridSizeY, 1, MPI_INTEGER,
            gridSizeYArray, 1, MPI_INTEGER, 0, cartComm);
    MPI_Gather(&xOffset, 1, MPI_INTEGER,
            xOffsetArray, 1, MPI_INTEGER, 0, cartComm);
    MPI_Gather(&yOffset, 1, MPI_INTEGER,
            yOffsetArray, 1, MPI_INTEGER, 0, cartComm);

    // On each processor, send grid data to process 0. Use non-blocking
    // communication to avoid deadlock.
    MPI_Request request;
    MPI_Isend(u, gridSizeX*gridSizeY, MPI_DOUBLE, 0, 0, cartComm, &request);
    
    // On process 0, loop over processes and receive the sub-block using
    // a derived data type
    if (myProcID == 0) {
        for (int proc = 0; proc < numProcs; ++proc) {
            MPI_Datatype subblockType;
            int count = gridSizeYArray[proc];
            int length = gridSizeXArray[proc];
            int stride = fullSizeX;
            MPI_Type_vector(count, length, stride, MPI_DOUBLE, &subblockType);
            MPI_Type_commit(&subblockType);
            double * recvPointer = &uAll[ yOffsetArray[proc] * fullSizeX + xOffsetArray[proc] ];
            printf("Recv from %d at offset X:%d Y:%d Z:%d, chunks of %d by %d by %d, out of %d by %d by %d\n", proc,
                    xOffsetArray[proc],
                    yOffsetArray[proc],
                    0,
                    gridSizeXArray[proc],
                    gridSizeYArray[proc],
                    0,
                    fullSizeX,
                    fullSizeY,
                    0);
            MPI_Status status;
            MPI_Recv(recvPointer, 1, subblockType, proc,
                    0, cartComm, &status);
            MPI_Type_free(&subblockType);
        }
    }

    MPI_Wait(&request, MPI_STATUS_IGNORE);

    // Output to file from proc 0
    if (myProcID == 0) {
        writeToFile(uAll, fullSizeX, fullSizeY, dx, dy);
    }

    // Delete arrays on proc 0
    if (myProcID == 0) {
        free(uAll);
        free(gridSizeXArray);
        free(gridSizeYArray);
        free(xOffsetArray);
        free(yOffsetArray);
    }

}
//-----------------------------------------------------------------------------------

void writeToFile(const double * u, int nx, int ny, double dx, double dy) {
    FILE* myFile = fopen("Heat2D.dat", "a");

    const int width = 8;
    const int prec = 5;

    for (int j = 0; j < ny; ++j) {
        double y = j*dy;
        for (int i = 0; i < nx; ++i) {
            double x = i*dx;
            double val = u[j * nx + i];
            char pattern[] = "%8.5f\t%8.5f\t%8.5f\n";
            fprintf(myFile, pattern, x, y, val);
        }
    }

    fclose(myFile);
}
//------------------------------------------------------------------------------------
