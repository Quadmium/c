% Clean Up
clear all;
close all;

% Define Number of Nodes and Lengths of Plane
nx = 100;
ny = 100;
Lx = 1.0;
Ly = 1.0;

% Set Number of Cells
nnx = nx+1;
nny = ny+1;

% Read in Data into three columns (x,y,T)
A = dlmread('Heat2D.dat');
u = A(:,3); 

% If there isn't a T for every grid point, something is wrong
assert(length(u) == nnx*nny);

% Create (x,y) plane, reshape T structure for plotting
x = linspace(0, Lx, nnx);
y = linspace(0, Ly, nny);
u = reshape(u, nnx, nny)';

% Plot Temperature Distribution in the Plane
surf(x,y,u);
xlabel('Position - x')
ylabel('Position - y')
title('Temperature Distribution')
