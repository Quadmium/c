#include "mpi.h" 
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Outputs.h"
#include "OutputsAsync.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

// Block 1
// Forward declarations
typedef struct {
    double meltingPoint, range, density, specificHeat, latentHeat;
} MProp;

void getLocalSize(MPI_Comm cartComm, int dim, int n, int * nLocal, int * offset);
void setToZero(int xSize, int ySize, int zSize, double * u);
void getXYZ(int i, int j, int k, int xOffset, int yOffset, int zOffset,
        double dx, double dy, double dz, double * x, double * y, double * z);
void setBCs(MPI_Comm cartComm, int xSize, int ySize, int zSize, int xOffset, int yOffset, int zOffset,
        double dx, double dy, double dz, double * u);
double bcFunc(double x, double y, double z);
void ForwardTime(unsigned long long step, int xSize, int ySize, int zSize,
        double dx, double dy, double dz, int xOffset, int yOffset, int zOffset, int nx, int ny, int nz, double dt, double maxTime, double D,
        const double * u, double * unew, int fixZBoundary, MProp *matProperties);
double SpecificHeat(double temperature, MProp *matProperties);
void setEqual(int xSize, int ySize, int zSize, const double * u1, double * u2);
void exchangeBoundaryData(MPI_Comm cartComm, int xSize, int ySize, int zSize, double * u, MPI_Datatype xyType, MPI_Datatype yzType, MPI_Datatype zxType);
void outputToFile(MPI_Comm cartComm, double * u, int nx, int ny, int nz, int gridSizeX, int gridSizeY, int gridSizeZ,
        int xOffset, int yOffset, int zOffset, double dx, double dy, double dz, int format, int* Conn, double* Coords, char* name);
void writeToFile(const double * u, int nx, int ny, int nz, double dx, double dy, double dz);
//--------------------------------------------------------------------------------
// Block 2

int onlyOnePoint = 1;
FILE* onePointFile = NULL;
int amOnePoint = 0;
int onePointX, onePointY, onePointZ;

int main(int argc, char **argv) {
    /*
     * Structure of mesh:
     * Y points to the top of this page ^
     * X points to the right of this page >
     * Z Points out of this page
     * 
     * Planes needed (right hand)
     * X-Y
     * Y-Z
     * Z-X
     */
    
    MPI_Init(&argc, &argv);
    
    int asyncOut = 1;
    
    char* outDir = argc < 2 ? "out" : argv[1];
    int dirLength = strlen(outDir);
    
    if(!asyncOut)
    {
        char cmd[10+dirLength];
        sprintf(cmd, "rm -rf %s", outDir);
        system(cmd);
    }
    char mkdircmd[15+dirLength];
    sprintf(mkdircmd, "mkdir -p %s", outDir);
    system(mkdircmd);
    
    mkdir(outDir, 0777);
    
    // Get my processor ID and number of processors
    int myProcID, numProcs;
    MPI_Comm_rank(MPI_COMM_WORLD, &myProcID);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
    
    //http://resources.yesican-science.ca/lpdd/g07/lp/sh_table.html
    MProp matProperties;/*
    matProperties.density = 8050; // kg/m^3
    matProperties.specificHeat = 450; // J/(kg*c))
    matProperties.latentHeat = 2.76*100000; // J/kg
    matProperties.meltingPoint = 1540; // c
    matProperties.range = 50; // c*/
    matProperties.density = 0.008050; // g/mm^3
    matProperties.specificHeat = 0.450; // J/(g*c))
    matProperties.latentHeat = 276; // J/g
    matProperties.meltingPoint = 1540; // c
    matProperties.range = 50; // c
    
    // Set some parameters
    double Lx = 10;//1.0;
    double Ly = 20;//2.0;
    double Lz = 2.5;//0.5;
    int nx = 60; // Number of cells in x direction (nnodesx = nx+1)
    int ny = 120; // Number of cells in y direction (nnodesy = ny+1);
    int nz = 5; // Number of cells in z direction (nnodesz = nz+1);
    
    int pointXInd = nx/2, pointYInd = 15, pointZInd = 5;
    
    // Compute some things
    double dx = Lx / nx;
    double dy = Ly / ny;
    double dz = Lz / nz;
    
    // Determine time step from Diffusivity
    // and grid size
    double D = 0.002; //Watt / (Meter Kelvin)
    
    if(argc >= 7)
    {
        if(myProcID == 0)
            printf("Found custom arguments. Format: name diffusivity density(g/mm^3) specificheat(J/(g*c)) latentheat(J/g) meltingpoint(c) range(c)\n");
        D = atof(argv[2]);
        matProperties.density = atof(argv[3]); // g/mm^3
        matProperties.specificHeat = atof(argv[4]); // J/(g*c))
        matProperties.latentHeat = atof(argv[5]); // J/g
        matProperties.meltingPoint = atof(argv[6]); // c
        matProperties.range = atof(argv[7]); // c
    }
    
    int outID = 0;
    if(argc >= 8)
        outID = atoi(argv[8]);
    
    double dtMax = 1.0 / 2 / (1/(dx*dx) + 1/(dy*dy) + 1/(dz*dz)) / D * (matProperties.density * matProperties.specificHeat); //(1.0 / 6.0)*(dx * dx / D);
    double dt = 0.004;//0.2 * dtMax;
    printf("%f\n", dtMax);
    
    double Tmax = 4;//1.5;
    unsigned long long Numstep = Tmax / dt;
    int saveModulus = Numstep / 400;//(Tmax*60);
    unsigned long long savedSteps;
    if(saveModulus == 0 || saveModulus == 1)
    {
        saveModulus = 1;
        savedSteps = Numstep;
    } else
        savedSteps = 1 + (Numstep-1)/saveModulus;
    // For save mod = 50, numStep = 200:
    // Print 0, 50, 100, 150, stop (k = 0-199 is 200 steps)
    // If 200 : 4 times if 201: 5 times
    double Time = 0.0;
    double PercComp = 0.0;
    
    /*if(myProcID == 0)
    {
        // Erase existing and write header info
        FILE* myFile = fopen("Heat3D.hsim", "w");
        int temp = nx+1;
        fwrite(&temp, sizeof(int), 1, myFile);
        temp = ny+1;
        fwrite(&temp, sizeof(int), 1, myFile);
        temp = nz+1;
        fwrite(&temp, sizeof(int), 1, myFile);
        fwrite(&Lx, sizeof(double), 1, myFile);
        fwrite(&Ly, sizeof(double), 1, myFile);
        fwrite(&Lz, sizeof(double), 1, myFile);
        fwrite(&Numstep, sizeof(unsigned long long), 1, myFile);
        fwrite(&savedSteps, sizeof(unsigned long long), 1, myFile);
        fwrite(&D, sizeof(double), 1, myFile);
        fwrite(&dt, sizeof(double), 1, myFile);
        fclose(myFile);
    }*/

    // Set up Cartesian topology
    int ndims = 3;
    int dims[3] = {0, 0, 0};
    MPI_Dims_create(numProcs, ndims, dims);
    int periods[3] = {0, 0, 0};
    int reorder = 1;
    MPI_Comm cartComm;
    MPI_Cart_create(MPI_COMM_WORLD, ndims, dims,
            periods, reorder, &cartComm);
    int cartRank;
    MPI_Comm_rank(cartComm, &cartRank);
    int coords[3];
    MPI_Cart_coords(cartComm, cartRank, 3, coords);

    // Compute size of my local grid.
    int nxLocal, nyLocal, nzLocal;
    int xOffset, yOffset, zOffset;
    getLocalSize(cartComm, 0, nx, &nxLocal, &xOffset);
    getLocalSize(cartComm, 1, ny, &nyLocal, &yOffset);
    getLocalSize(cartComm, 2, nz, &nzLocal, &zOffset);

    // Allocate memory for mesh -- (nxLocal+2) by (nyLocal+2) by (nzLocal+2) to account
    // for boundary or ghost nodes  
    int gridSizeX = nxLocal + 2;
    int gridSizeY = nyLocal + 2;
    int gridSizeZ = nzLocal + 2;
    double * u = malloc(sizeof (double) * gridSizeX * gridSizeY * gridSizeZ);
    double * unew = malloc(sizeof (double) * gridSizeX * gridSizeY * gridSizeZ);
    
    if(myProcID == 0)
    {
        printf("MX:%d,MY:%d,MZ:%d,LX:%f,LY:%f,LZ:%f,NSTEP:%llu,SSTEP:%llu,DIF:%f,DT:%f\n", gridSizeX, gridSizeY, gridSizeZ, Lx,
            Ly, Lz, Numstep, savedSteps, D, dt);
    }

    // Set up initial state: zeros except for boundary condition.
    setToZero(gridSizeX, gridSizeY, gridSizeZ, u);
    setToZero(gridSizeX, gridSizeY, gridSizeZ, unew);   
    setBCs(cartComm, gridSizeX, gridSizeY, gridSizeZ, xOffset, yOffset, zOffset, dx, dy, dz, u);
    setBCs(cartComm, gridSizeX, gridSizeY, gridSizeZ, xOffset, yOffset, zOffset, dx, dy, dz, unew);
    
    // Define row and column data types
    MPI_Datatype xyType, yzType, zxType;
    MPI_Type_contiguous(gridSizeX * gridSizeY, MPI_DOUBLE, &xyType);
    MPI_Type_vector(gridSizeY * gridSizeZ, 1, gridSizeX, MPI_DOUBLE, &yzType);
    MPI_Type_vector(gridSizeZ, gridSizeX, gridSizeX * gridSizeY, MPI_DOUBLE, &zxType);
    MPI_Type_commit(&xyType);
    MPI_Type_commit(&yzType);
    MPI_Type_commit(&zxType);
    
    int* Conn = NULL;
    double* Coords = NULL;
    long fileOffset;
    if(myProcID == 0 && !onlyOnePoint)
    {
        int np = (nx+1)*(ny+1)*(nz+1);
        Conn = malloc(sizeof(int) * 8*nx*ny*nz);
        Coords = malloc(sizeof(double) * np*3);
        CreateConn(Conn, nx, ny, nz);
        CreateCoords(Coords, nx, ny, nz, Lx, Ly, Lz);
        
        if(asyncOut)
        {
            /*printf("Pre-writing steps (%llu)...\n", savedSteps);
            
            char cmd[300];
            SetupVTUAsync(Coords, Conn, "out/template", nx, ny, nz);
            //sprintf(cmd, "(for FILE in `seq 0 %llu`; do dd status=none if=out/template of=out/Heat3DPara$FILE.vtu; done; rm out/template) &", savedSteps-1);
            //sprintf(cmd, "(for FILE in `seq 0 %llu`; do cat out/template > out/Heat3DPara$FILE.vtu; done; rm out/template) &", savedSteps-1);
            sprintf(cmd, "(for FILE in `seq 0 %llu`; do cp out/template out/Heat3DPara$FILE.vtu; done; rm out/template) &", savedSteps-1);
            //system(cmd);*/
            
            printf("Pre-writing steps (%llu)...\n", savedSteps); 
            char dep1[100+dirLength], dep2[100+dirLength], dep3[100+dirLength];
            sprintf(dep1, "%s/Heat3DPara%llu.vtu", outDir, savedSteps-1);
            sprintf(dep2, "%s/Heat3DPara%llu.vtu", outDir, savedSteps);
            sprintf(dep3, "%s/Heat3DPara0.vtu", outDir);
            char templatePath[100+dirLength];
            sprintf(templatePath, "%s/template", outDir);
            fileOffset = SetupVTUAsync(Coords, Conn, templatePath, nx, ny, nz);
            int failed = access(dep1, F_OK ) == -1 || access(dep2, F_OK ) != -1 || access(dep3, F_OK ) == -1;
            if(!failed)
            {
                char firstFilePath[100+dirLength];
                sprintf(firstFilePath, "%s/Heat3DPara0.vtu", outDir);
                FILE* firstOut = fopen(firstFilePath, "rb");
                FILE* template = fopen(templatePath, "rb");
                
                int pos = 0;
                while(pos++ < fileOffset && !failed)
                    if(fgetc(firstOut) != fgetc(template))
                        failed = 1;
                    
                fclose(template);
                fclose(firstOut);
            }
            
            if(failed)
            {
                printf("Template invalid, regenerating file to %s...\n", outDir); 
                char cmd[300+2*dirLength], cmd2[100+2*dirLength];
                sprintf(cmd2, "rm -rf %s; mkdir %s", outDir, outDir);
                system(cmd2);
                fileOffset = SetupVTUAsync(Coords, Conn, templatePath, nx, ny, nz);
                sprintf(cmd, "for FILE in `seq 0 %llu`; do cp %s/template %s/Heat3DPara$FILE.vtu; done;", savedSteps-1, outDir, outDir);
                system(cmd);
            }
            printf("Template valid at %s, continuing...\n", outDir);
        }
    }
    if(!onlyOnePoint)
        MPI_Bcast(&fileOffset, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    int percInt = 0;
                
    for (unsigned long long k = 0; k < Numstep; k++) {
        
        int topLayer = coords[2] == dims[2] - 1;
        
        // Master Core Prints out Progress Check
        if (k % saveModulus == 0)
        {
            if(myProcID == 0 && percInt != (int)PercComp)
            {
                printf("Progress Check and save: %d Percent Done!\n", percInt);
                percInt = (int)PercComp;
            }
            char name[200+dirLength];
            sprintf(name, "%s/Heat3DPara%llu.vtu", outDir, k / saveModulus);
            if(onlyOnePoint)
            {
                if(amOnePoint)
                {
                    if(onePointFile == NULL)
                    {
                        char name[200+dirLength];
                        sprintf(name, "%s/Heat3DPara%d.opt", outDir, outID);
                        onePointFile = fopen(name, "w");
                    }
                    //printf("%d %d %d\n", onePointX, onePointY, onePointZ);
                    fprintf(onePointFile, "%f\n", u[onePointZ*gridSizeX*gridSizeY + onePointY*gridSizeX + onePointX]);
                }
            }
            else if(asyncOut)
                WriteVTUAsync(xOffset, yOffset, zOffset, gridSizeX, gridSizeY, gridSizeZ, nx, ny, nz, u, name, fileOffset);
            else
                outputToFile(cartComm, u, nx, ny, nz, gridSizeX, gridSizeY, gridSizeZ, xOffset, yOffset, zOffset, dx, dy, dz, 1, Conn, Coords, name);
        }
        
        //AnimateLaser(k, gridSizeX, gridSizeY, gridSizeZ, xOffset, yOffset, zOffset, dx, dy, dz, nx, ny, nz, dt, D, u, unew, topLayer);
        
        // Forward Euler
        ForwardTime(k, gridSizeX, gridSizeY, gridSizeZ, dx, dy, dz, xOffset, yOffset, zOffset, nx, ny, nz, dt, Tmax, D, u, unew, topLayer, &matProperties);
        
        // Exchange process boundary data in unew
        exchangeBoundaryData(cartComm, gridSizeX, gridSizeY, gridSizeZ, unew, xyType, yzType, zxType);

        // Set u equal to unew
        setEqual(gridSizeX, gridSizeY, gridSizeZ, unew, u);

        // Compute Time and Percent Complete
        Time = dt * (k + 1);
        PercComp = 100.0 * (1.0 * k) / Numstep;

    }

    // Clean up
    free(u);
    free(unew);
    
    if(onlyOnePoint && amOnePoint)
        fclose(onePointFile);
    
    if(myProcID == 0 && !onlyOnePoint)
    {
        free(Conn);
        free(Coords);
    }
    
    MPI_Type_free(&xyType);
    MPI_Type_free(&yzType);
    MPI_Type_free(&zxType);

    MPI_Finalize();
    return 0;
}
//-------------------------------------------------------------------------------------
//Block 5

void getLocalSize(MPI_Comm cartComm, int dim, int n, int * nLocal, int * offset) {
    // Get my process coordinates
    int cartDims[3];
    int periods[3];
    int cartCoords[3];
    MPI_Cart_get(cartComm, 3, cartDims, periods, cartCoords);

    // Compute number of local interior nodes, and offset
    // global_I = local_i + offset
    *nLocal = (n - 1) / cartDims[dim];
    *offset = cartCoords[dim] * *nLocal;

    // Account for non-uniform distribution on last proc: offset + nlocal + 1 = n
    if (cartCoords[dim] == cartDims[dim] - 1) {
        *nLocal = n - 1 - *offset;
    }
}
//---------------------------------------------------------------------------------------
// Block 6

void setToZero(int xSize, int ySize, int zSize, double * u) {
    memset(u, 0, sizeof(double) * xSize * ySize * zSize);
}
//---------------------------------------------------------------------------------------
// Block 7

void setBCs(MPI_Comm cartComm, int xSize, int ySize, int zSize, int xOffset, int yOffset, int zOffset,
        double dx, double dy, double dz, double * u) {
    // Get my process coordinates
    int cartDims[3];
    int periods[3];
    int cartCoords[3];
    MPI_Cart_get(cartComm, 3, cartDims, periods, cartCoords);

    double x, y, z;
    
    for(int k=0; k<zSize; k++)
    for(int j=0; j<ySize; j++)
    for(int i=0; i<xSize; i++)
    {
        getXYZ(i, j, k, xOffset, yOffset, zOffset, dx, dy, dz, &x, &y, &z);
        //u[k*ySize*xSize + j*xSize + i] = z >= 0.34 && z <= 0.41 || z >= 0.09 && z <= 0.16;
    }
}
//-----------------------------------------------------------------------------
// Block 8

void getXYZ(int i, int j, int k, int xOffset, int yOffset, int zOffset,
        double dx, double dy, double dz, double * x, double * y, double * z) {
    *x = (i + xOffset) * dx;
    *y = (j + yOffset) * dy;
    *z = (k + zOffset) * dz;
}
//----------------------------------------------------------------------------
// Block 9

double bcFunc(double x, double y, double z) {
    double u = 1;
    return u;
}

//--------------------------------------------------------------------------------
// Block 10
void ForwardTime(unsigned long long step, int xSize, int ySize, int zSize,
        double dx, double dy, double dz, int xOffset, int yOffset, int zOffset, int nx, int ny, int nz, double dt, double maxTime, double D,
        const double * u, double * unew, int fixZBoundary, MProp *matProperties) {
    double avgMesh = nx*dx;
    double spread = 1 * avgMesh/10.0;
    double heatTarget = 50; // Watts
    double heatTime = 1;
    double intensity = heatTarget / heatTime;
    
    double curTime = step * dt;
    //double laserPosWorldY = 0.2 + 1.6 / 2 * (curTime * 2 / 5);
    double laserTime = 3;
    double laserPosWorldY = dy + (ny-2) * dy * curTime / laserTime;
    double laserPosWorldX = nx * dx / 2;
    
    double dx2 = dx*dx;
    double dy2 = dy*dy;
    double dz2 = dz*dz;
    
    for (int k = 1; k < zSize - 1; k++)
    for (int j = 1; j < ySize - 1; j++) 
    for (int i = 1; i < xSize - 1; i++)
    {
        int ind = ySize * xSize * k + xSize * j + i;
        
        double uE = u[ySize * xSize * k + xSize * j + (i + 1)];
        double uW = u[ySize * xSize * k + xSize * j + (i - 1)];
        double uN = u[ySize * xSize * k + xSize * (j + 1) + i];
        double uS = u[ySize * xSize * k + xSize * (j - 1) + i];
        double uI = u[ySize * xSize * (k + 1) + xSize * j + i];
        double uO = u[ySize * xSize * (k - 1) + xSize * j + i];
        double uC = u[ySize * xSize * k + xSize * j + i ];
        double xDer = (uE+uW-2*uC)/dx2;
        double yDer = (uN+uS-2*uC)/dy2;
        double zDer = (uO+uI-2*uC)/dz2;
        double laserAdded = 0;
        
        if(fixZBoundary && k==zSize-2)
        {
            double x, y, z;
            getXYZ(i, j, zSize-2, xOffset, yOffset, zOffset, dx, dy, dz, &x, &y, &z);
            double rX = (laserPosWorldX - x);
            double rY = (laserPosWorldY - y);
            double radius2ToLaser =  rX * rX + rY * rY;
            laserAdded = dt * intensity * exp(-radius2ToLaser / (2*spread*spread));
            if(rX <= dx/2 && (y - 16) <= dy/2 && rX >= -dx/2 && y-16 >= -dy/2 && onlyOnePoint)
            {
                amOnePoint = 1;
                onePointX = i;
                onePointY = j;
                onePointZ = k+1;
            }
        }
        
        unew[ind] = uC + (D*dt*(xDer + yDer + zDer) + laserAdded)/(matProperties->density * SpecificHeat(uC, matProperties));
    }
    
    if(fixZBoundary)
    {
        int k = zSize - 1;
        for (int j = 1; j < ySize - 1; j++) 
        for (int i = 1; i < xSize - 1; i++)
            unew[ySize * xSize * k + xSize * j + i] = unew[ySize * xSize * (k-1) + xSize * j + i];
    }
}

double SpecificHeat(double temperature, MProp *matProperties)
{
    double low = matProperties->meltingPoint - matProperties->range/2;
    double high = matProperties->meltingPoint + matProperties->range/2;
    
    if(temperature >= low && temperature <= high){
        return matProperties->specificHeat + matProperties->latentHeat / matProperties->range;}
    else
        return matProperties->specificHeat;
}

//-----------------------------------------------------------------------------------
// Block 11

void setEqual(int xSize, int ySize, int zSize, const double * u1, double * u2) {
    memcpy(u2, u1, sizeof(double) * xSize * ySize * zSize);
}
//-----------------------------------------------------------------------------------
// Block 12

void exchangeBoundaryData(MPI_Comm cartComm, int xSize, int ySize, int zSize, double * u, MPI_Datatype xyType, MPI_Datatype yzType, MPI_Datatype zxType) {

    // Get the processes at right, left, up, down, out, and in
    int procR, procL, procU, procD, procO, procI;
    MPI_Cart_shift(cartComm, 0, 1, &procL, &procR);
    MPI_Cart_shift(cartComm, 1, 1, &procD, &procU);
    MPI_Cart_shift(cartComm, 2, 1, &procI, &procO);
    
    // MPI_PROC_NULL for boundary cases and MPI_Sendrecv on it succeeds immediately

    // 1: Send XY+ to out receive XY- from in
    // 2: Send XY- to in receive XY+ from out
    // 3: Send YZ+ to right receive YZ- from left
    // 4: Send YZ- to left receive YZ+ from right
    // 5: Send ZX+ to up receive ZX- from down
    // 6: Send ZX- to down receive ZX+ from top
    
    // 1: Send XY+ to out receive XY- from in
    MPI_Sendrecv(&u[(zSize-2)*ySize*xSize], 1, xyType, procO, 0,
                 &u[0], 1, xyType, procI, 0,
                 cartComm, MPI_STATUS_IGNORE);
    
    // 2: Send XY- to in receive XY+ from out
    MPI_Sendrecv(&u[ySize*xSize], 1, xyType, procI, 0,
                 &u[(zSize-1)*ySize*xSize], 1, xyType, procO, 0,
                 cartComm, MPI_STATUS_IGNORE);
    
    // 3: Send YZ+ to right receive YZ- from left
    MPI_Sendrecv(&u[xSize-2], 1, yzType, procR, 0,
                 &u[0], 1, yzType, procL, 0,
                 cartComm, MPI_STATUS_IGNORE);
    
    // 4: Send YZ- to left receive YZ+ from right
    MPI_Sendrecv(&u[1], 1, yzType, procL, 0,
                 &u[xSize-1], 1, yzType, procR, 0,
                 cartComm, MPI_STATUS_IGNORE);
    
    // 5: Send ZX+ to up receive ZX- from down
    MPI_Sendrecv(&u[(ySize-2)*xSize], 1, zxType, procU, 0,
                 &u[0], 1, zxType, procD, 0,
                 cartComm, MPI_STATUS_IGNORE);
    
    // 6: Send ZX- to down receive ZX+ from top
    MPI_Sendrecv(&u[xSize], 1, zxType, procD, 0,
                 &u[(ySize-1)*xSize], 1, zxType, procU, 0,
                 cartComm, MPI_STATUS_IGNORE);
}
//---------------------------------------------------------------------------------------
// Block 13

void outputToFile(MPI_Comm cartComm, double * u, int nx, int ny, int nz, int gridSizeX, int gridSizeY, int gridSizeZ,
        int xOffset, int yOffset, int zOffset, double dx, double dy, double dz, int format, int* Conn, double* Coords, char* name) {
    int myProcID, numProcs;
    MPI_Comm_rank(cartComm, &myProcID);
    MPI_Comm_size(cartComm, &numProcs);

    // On process 0, allocate data for entire u array, as well as arrays
    // to hold grid sizes and offsets gathered from individual procs
    double * uAll;
    int *gridSizeXArray, *gridSizeYArray, *gridSizeZArray, *xOffsetArray, *yOffsetArray, *zOffsetArray;
    int fullSizeX = nx + 1;
    int fullSizeY = ny + 1;
    int fullSizeZ = nz + 1;
    if (myProcID == 0) {
        uAll = malloc(sizeof (double) * fullSizeX * fullSizeY * fullSizeZ);
        gridSizeXArray = malloc(sizeof (int) * numProcs);
        gridSizeYArray = malloc(sizeof (int) * numProcs);
        gridSizeZArray = malloc(sizeof (int) * numProcs);
        xOffsetArray = malloc(sizeof (int) * numProcs);
        yOffsetArray = malloc(sizeof (int) * numProcs);
        zOffsetArray = malloc(sizeof (int) * numProcs);
    }
    
    // Gather grid sizes and offsets
    MPI_Gather(&gridSizeX, 1, MPI_INTEGER,
            gridSizeXArray, 1, MPI_INTEGER, 0, cartComm);
    MPI_Gather(&gridSizeY, 1, MPI_INTEGER,
            gridSizeYArray, 1, MPI_INTEGER, 0, cartComm);
    MPI_Gather(&gridSizeZ, 1, MPI_INTEGER,
            gridSizeZArray, 1, MPI_INTEGER, 0, cartComm);
    MPI_Gather(&xOffset, 1, MPI_INTEGER,
            xOffsetArray, 1, MPI_INTEGER, 0, cartComm);
    MPI_Gather(&yOffset, 1, MPI_INTEGER,
            yOffsetArray, 1, MPI_INTEGER, 0, cartComm);
    MPI_Gather(&zOffset, 1, MPI_INTEGER,
            zOffsetArray, 1, MPI_INTEGER, 0, cartComm);

    // On each processor, send grid data to process 0. Use non-blocking
    // communication to avoid deadlock.
    MPI_Request request;
    MPI_Isend(u, gridSizeX*gridSizeY*gridSizeZ, MPI_DOUBLE, 0, 0, cartComm, &request);
    
    // On process 0, loop over processes and receive the sub-block using
    // a derived data type
    if (myProcID == 0) {
        for (int proc = 0; proc < numProcs; ++proc) {
            MPI_Datatype subblockType;
            int count = gridSizeYArray[proc];
            int length = gridSizeXArray[proc];
            int stride = fullSizeX;
            MPI_Type_vector(count, length, stride, MPI_DOUBLE, &subblockType);
            MPI_Type_commit(&subblockType);
            MPI_Datatype upZType;
            count = gridSizeZArray[proc];
            length = 1;
            stride = fullSizeY * fullSizeX * sizeof(double);
            MPI_Type_create_hvector(count, length, stride, subblockType, &upZType);
            MPI_Type_commit(&upZType);
            
            /*printf("Recv from %d at offset X:%d Y:%d Z:%d, chunks of %d by %d by %d, out of %d by %d by %d\n", proc,
                    xOffsetArray[proc],
                    yOffsetArray[proc],
                    zOffsetArray[proc],
                    gridSizeXArray[proc],
                    gridSizeYArray[proc],
                    gridSizeZArray[proc],
                    fullSizeX,
                    fullSizeY,
                    fullSizeZ);*/
            
            double * recvPointer = &uAll[ zOffsetArray[proc] * fullSizeY * fullSizeX + 
                                          yOffsetArray[proc] * fullSizeX + xOffsetArray[proc] ];
            MPI_Status status;
            MPI_Recv(recvPointer, 1, upZType, proc,
                    0, cartComm, &status);
            MPI_Type_free(&upZType);
            MPI_Type_free(&subblockType);
        }
    }

    MPI_Wait(&request, MPI_STATUS_IGNORE);

    // Output to file from proc 0
    if (myProcID == 0) {
        if(format == 0)
            writeToFile(uAll, fullSizeX, fullSizeY, fullSizeZ, dx, dy, dz);
        else if(format == 1)
            WriteVTU(Coords, Conn, name, nx, ny, nz, uAll);
    }

    // Delete arrays on proc 0
    if (myProcID == 0) {
        free(uAll);
        free(gridSizeXArray);
        free(gridSizeYArray);
        free(gridSizeZArray);
        free(xOffsetArray);
        free(yOffsetArray);
        free(zOffsetArray);
    }

}
//-----------------------------------------------------------------------------------

void writeToFile(const double * u, int nx, int ny, int nz, double dx, double dy, double dz) {
    FILE* myFile = fopen("Heat3D.hsim", "a");

    for(int k = 0; k < nz; k++)
    for(int j = 0; j < ny; j++)
    for(int i = 0; i < nx; i++)
    {
        double val = u[k * ny * nx + j * nx + i];
        fwrite(&val, sizeof(double), 1, myFile);
    }
    
    fclose(myFile);
}
//------------------------------------------------------------------------------------