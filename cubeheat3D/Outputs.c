#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "Outputs.h"

static long fsize(const char* file);

void CreateConn(int *Conn, int Nx, int Ny, int Nz)
{
  int inode;
  for (int k = 0; k < Nz; k++)
  {
    for (int j = 0; j < Ny; j++)
    {
      for(int i = 0; i < Nx; i++)
      {   
        inode = i + j*Nx + k*(Ny*Nx);
        
        Conn[ inode*8   ] = i +         j*(Nx+1) + k*(Nx+1)*(Ny+1); 
        Conn[ inode*8+1 ] = i + 1 +     j*(Nx+1) + k*(Nx+1)*(Ny+1);
        Conn[ inode*8+2 ] = i + 1 + (j+1)*(Nx+1) + k*(Nx+1)*(Ny+1);
        Conn[ inode*8+3 ] = i +     (j+1)*(Nx+1) + k*(Nx+1)*(Ny+1);
        
        Conn[ inode*8+4 ] = i +         j*(Nx+1) + (k+1)*((Ny+1)*(Nx+1));
        Conn[ inode*8+5 ] = i + 1 +     j*(Nx+1) + (k+1)*((Ny+1)*(Nx+1));
        Conn[ inode*8+6 ] = i + 1 + (j+1)*(Nx+1) + (k+1)*((Ny+1)*(Nx+1));
        Conn[ inode*8+7 ] = i +     (j+1)*(Nx+1) + (k+1)*((Ny+1)*(Nx+1));
      }//end for(i)
    }//end for(j)
  }//end for(k) 
}// end function "CreateConn"

void CreateCoords(double *Coords, int Nx, int Ny, int Nz, double Lx, double Ly, double Lz)
{
  int inode;
  double dx, dy, dz;
  dx = Lx/(double)Nx;
  dy = Ly/(double)Ny;
  dz = Lz/(double)Nz;
  
  for(int k = 0; k < Nz+1; k++)
  {
    for(int j = 0; j < Ny+1; j++)
    {
      for(int i = 0; i < Nx+1; i++)
      {
        inode = i + j*(Nx+1) + k*(Nx+1)*(Ny+1);
        Coords[inode*3  ] = (double)i * dx;
        Coords[inode*3+1] = (double)j * dy;
        Coords[inode*3+2] = (double)k * dz;
      }//end for(i)
    }//end for(j)
  }//end for(k)
}//end function CreateCoords

void WriteVTU(double *Coords, int *Conn, const char * filename,
              int Nx, int Ny, int Nz, double *u)
{
  // File writes
  unsigned int offset = 0;
  int nb = 0;
  char string_np [80];
  char string_nel [80];
  char string_offset[80];
  int np = (Nx+1)*(Ny+1)*(Nz+1);
  int nel = Nx * Ny * Nz;
  FILE * outfile = fopen(filename, "w");

  fprintf(outfile, " <VTKFile type=\"UnstructuredGrid\" version=\"0.1\"    byte_order=\"LittleEndian\">");
  fprintf(outfile, " <UnstructuredGrid>");
  sprintf(string_np, "%d", np);
  sprintf(string_nel, "%d", nel);

  // Write out temperature data
  fprintf(outfile, " <Piece NumberOfPoints=\"%s\" NumberOfCells=\"%s\">",
          string_np, string_nel);
  fprintf(outfile, " <PointData>");
  sprintf(string_offset, "%u", offset);
  fprintf(outfile, " <DataArray type=\"Float64\" Name=\"Temp\" format=\"appended\" offset=\"%s\" />", string_offset);
  offset = offset + sizeof(double)*np + sizeof(offset);
  sprintf(string_offset, "%u", offset);
  fprintf(outfile, " </PointData>");
  
  // Write out fake cell datas
  fprintf(outfile, " <CellData> </CellData>");

  // Write out coordinate data
  fprintf(outfile, " <Points>");
  fprintf(outfile, " <DataArray type=\"Float64\" NumberOfComponents=\"3\" format=\"appended\" offset=\"%s\" />", string_offset);
  fprintf(outfile, " </Points>");
  
  // Write out connectivity data
  offset = offset + sizeof(double)*np*3 + sizeof(offset);
  sprintf(string_offset, "%u", offset);
  fprintf(outfile, " <Cells>");
  fprintf(outfile, " <DataArray type=\"Int32\" Name=\"connectivity\" format=\"appended\" offset=\"%s\" />", string_offset);

  // Write node offset data
  offset = offset + sizeof(int)*8*nel + sizeof(offset);
  sprintf(string_offset, "%u", offset);
  fprintf(outfile, " <DataArray type=\"Int32\" Name=\"offsets\"  format=\"appended\" offset=\"%s\" />", string_offset);

  // Write cell types
  offset = offset + sizeof(int)*nel + sizeof(offset);
  sprintf(string_offset, "%u", offset);
  fprintf(outfile, " <DataArray type=\"Int32\" Name=\"types\" format=\"appended\" offset=\"%s\" />", string_offset);

  // Exit Cell Data
  fprintf(outfile, " </Cells>");
 
  // Exit Point Data
  fprintf(outfile, " </Piece>");

  // Exit grid type
  fprintf(outfile, " </UnstructuredGrid>");

  // Begin to append data
  fprintf(outfile, " <AppendedData encoding=\"raw\">_");
  
  // Write out temperature data
  nb = sizeof(double)*np;
  fwrite(&nb,sizeof(int), 1, outfile);
  fwrite(u, sizeof(double), np, outfile);

  // Write out the coordinates
  nb = sizeof(double)*np*3;
  fwrite(&nb,sizeof(int), 1, outfile);
  for(int i = 0; i < np; i++)
  {
    fwrite(&Coords[i*3  ], sizeof(double), 1, outfile);
    fwrite(&Coords[i*3+1], sizeof(double), 1, outfile);
    fwrite(&Coords[i*3+2], sizeof(double), 1, outfile);
  }//end for(i)

  // Write out connectivity
  nb = sizeof(int)*nel*8;
  fwrite(&nb, sizeof(int), 1, outfile);
  for (int i = 0; i < nel; i++)
  {
    fwrite(&Conn[i*8  ], sizeof(int), 1, outfile);
    fwrite(&Conn[i*8+1], sizeof(int), 1, outfile);
    fwrite(&Conn[i*8+2], sizeof(int), 1, outfile);
    fwrite(&Conn[i*8+3], sizeof(int), 1, outfile);
    fwrite(&Conn[i*8+4], sizeof(int), 1, outfile);
    fwrite(&Conn[i*8+5], sizeof(int), 1, outfile);
    fwrite(&Conn[i*8+6], sizeof(int), 1, outfile);
    fwrite(&Conn[i*8+7], sizeof(int), 1, outfile);
  }//end for(i)

  // Write out node offsets
  nb = sizeof(int)*nel;
  fwrite(&nb, sizeof(int), 1, outfile);
  for (int i = 0; i < nel ; i++)
  {
    int el_offset = 8*(i+1);
    fwrite(&el_offset, sizeof(int), 1, outfile);
  }//end for(i)

  // Write element types
  nb = sizeof(int) * nel;
  fwrite(&nb, sizeof(int), 1, outfile);
  int el_type = 12;
  for(int i = 0; i < nel; i++)
  {
    fwrite(&el_type, sizeof(int), 1, outfile);
  }//end for(i)

  // Exit Appended Data
  fprintf(outfile, " </AppendedData>");
  fprintf(outfile, " </VTKFile>");

  fclose(outfile);
}//end WriteOutput function

/*
int example()
{
  int Nx = 50;
  int Ny = 50;
  int Nz = 50;
  int np = (Nx+1)*(Ny+1)*(Nz+1);
  int nel = Nx * Ny * Nz;
  int Conn[8*Nx*Ny*Nz];
  double u[(Nx+1)*(Ny+1)*(Nz+1)];
  double Coords[np*3];
  const char * filename = "test_output21.vtu";

  // Prescribe some initial condition
  for (int k = 0; k < Nz+1; k++)
  {
    for( int j = 0; j < Ny+1; j++)
    {
      for (int i = 0; i < Nx+1; i++)
      {
        u[i + j*(Nx+1) + k*(Nx+1)*(Ny+1)] = 2.0 + 10.0*(double)i + 
                                                   5.0*(double)j +
                                                  15.9*(double)k;
      }//end for(i)
    }//end for(j)
  }//end for(k)

  // Create general mesh
  CreateConn(Conn, Nx, Ny, Nz);
  CreateCoords(Coords, Nx, Ny, Nz);
  WriteVTU(Coords, Conn, filename, Nx, Ny, Nz, u);

  free(Conn);
  return 0;
}*/

static long fsize(const char* file)
{
    FILE * f = fopen(file, "rb");
    fseek(f, 0, SEEK_END);
    long len = ftell(f);
    fclose(f);
    return len;
}
