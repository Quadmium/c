void CreateConn(int *Conn, int Nx, int Ny, int Nz);
void CreateCoords(double *Coords, int Nx, int Ny, int Nz, double Lx, double Ly, double Lz);
void WriteVTU(double *Coords, int *Conn, const char * filename,
              int Nx, int Ny, int Nz, double *u);