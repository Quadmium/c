#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "OutputsAsync.h"

static long fsize(const char* file);

long SetupVTUAsync(double *Coords, int *Conn, const char * filename,
        int Nx, int Ny, int Nz) 
{ 
    // File writes
    unsigned int offset = 0;
    int nb = 0;
    char string_np [80];
    char string_nel [80];
    char string_offset[80];
    int np = (Nx + 1)*(Ny + 1)*(Nz + 1);
    int nel = Nx * Ny * Nz;
    FILE * outfile = fopen(filename, "w");

    fprintf(outfile, " <VTKFile type=\"UnstructuredGrid\" version=\"0.1\"    byte_order=\"LittleEndian\">");
    fprintf(outfile, " <UnstructuredGrid>");
    sprintf(string_np, "%d", np);
    sprintf(string_nel, "%d", nel);

    // Write out temperature data
    fprintf(outfile, " <Piece NumberOfPoints=\"%s\" NumberOfCells=\"%s\">",
            string_np, string_nel);
    fprintf(outfile, " <PointData>");
    sprintf(string_offset, "%u", offset);
    fprintf(outfile, " <DataArray type=\"Float64\" Name=\"Temp\" format=\"appended\" offset=\"%s\" />", string_offset);
    offset = offset + sizeof(double)*np + sizeof(offset);
    sprintf(string_offset, "%u", offset);
    fprintf(outfile, " </PointData>");

    // Write out fake cell datas
    fprintf(outfile, " <CellData> </CellData>");

    // Write out coordinate data
    fprintf(outfile, " <Points>");
    fprintf(outfile, " <DataArray type=\"Float64\" NumberOfComponents=\"3\" format=\"appended\" offset=\"%s\" />", string_offset);
    fprintf(outfile, " </Points>");

    // Write out connectivity data
    offset = offset + sizeof (double)*np * 3 + sizeof (offset);
    sprintf(string_offset, "%u", offset);
    fprintf(outfile, " <Cells>");
    fprintf(outfile, " <DataArray type=\"Int32\" Name=\"connectivity\" format=\"appended\" offset=\"%s\" />", string_offset);

    // Write node offset data
    offset = offset + sizeof (int)*8 * nel + sizeof (offset);
    sprintf(string_offset, "%u", offset);
    fprintf(outfile, " <DataArray type=\"Int32\" Name=\"offsets\"  format=\"appended\" offset=\"%s\" />", string_offset);

    // Write cell types
    offset = offset + sizeof (int)*nel + sizeof (offset);
    sprintf(string_offset, "%u", offset);
    fprintf(outfile, " <DataArray type=\"Int32\" Name=\"types\" format=\"appended\" offset=\"%s\" />", string_offset);

    // Exit Cell Data
    fprintf(outfile, " </Cells>");

    // Exit Point Data
    fprintf(outfile, " </Piece>");

    // Exit grid type
    fprintf(outfile, " </UnstructuredGrid>");

    // Begin to append data
    fprintf(outfile, " <AppendedData encoding=\"raw\">_");

    // Skip this step:
    // Write out temperature data
    nb = sizeof(double)*np;
    fwrite(&nb, sizeof (int), 1, outfile);
    //fwrite(u, sizeof (double), np, outfile);
    
    // Use to get async binary location   
    fclose(outfile);
    long fileOffset = fsize(filename);
    outfile = fopen(filename, "r+");
    fseek(outfile, 0, SEEK_END);
    fseek(outfile, sizeof (double) * np, SEEK_CUR);
    
    // Write out the coordinates
    nb = sizeof (double)*np * 3;
    fwrite(&nb, sizeof (int), 1, outfile);
    for (int i = 0; i < np; i++) {
        fwrite(&Coords[i * 3 ], sizeof (double), 1, outfile);
        fwrite(&Coords[i * 3 + 1], sizeof (double), 1, outfile);
        fwrite(&Coords[i * 3 + 2], sizeof (double), 1, outfile);
    }//end for(i)

    // Write out connectivity
    nb = sizeof (int)*nel * 8;
    fwrite(&nb, sizeof (int), 1, outfile);
    for (int i = 0; i < nel; i++) {
        fwrite(&Conn[i * 8 ], sizeof (int), 1, outfile);
        fwrite(&Conn[i * 8 + 1], sizeof (int), 1, outfile);
        fwrite(&Conn[i * 8 + 2], sizeof (int), 1, outfile);
        fwrite(&Conn[i * 8 + 3], sizeof (int), 1, outfile);
        fwrite(&Conn[i * 8 + 4], sizeof (int), 1, outfile);
        fwrite(&Conn[i * 8 + 5], sizeof (int), 1, outfile);
        fwrite(&Conn[i * 8 + 6], sizeof (int), 1, outfile);
        fwrite(&Conn[i * 8 + 7], sizeof (int), 1, outfile);
    }//end for(i)

    // Write out node offsets
    nb = sizeof (int)*nel;
    fwrite(&nb, sizeof (int), 1, outfile);
    for (int i = 0; i < nel; i++) {
        int el_offset = 8 * (i + 1);
        fwrite(&el_offset, sizeof (int), 1, outfile);
    }//end for(i)

    // Write element types
    nb = sizeof (int) * nel;
    fwrite(&nb, sizeof (int), 1, outfile);
    int el_type = 12;
    for (int i = 0; i < nel; i++) {
        fwrite(&el_type, sizeof (int), 1, outfile);
    }//end for(i)

    // Exit Appended Data
    fprintf(outfile, " </AppendedData>");
    fprintf(outfile, " </VTKFile>");

    fclose(outfile);
    
    return fileOffset;
}

void WriteVTUAsync(int xOffset, int yOffset, int zOffset, int gridSizeX, int gridSizeY, int gridSizeZ, 
        int nx, int ny, int nz, double* u, char* name, long fileOffset)
{
    // File writes
    int nb = 0;
    int np = (nx + 1)*(ny + 1)*(nz + 1);
    FILE * outfile = fopen(name, "r+b");
    while(outfile == NULL)
        outfile = fopen(name, "r+b");
    
    fseek(outfile, fileOffset, SEEK_SET);
    
    int fullSizeX = nx + 1;
    int fullSizeY = ny + 1;
    int fullSizeZ = nz + 1;
    
    fseek(outfile, sizeof(double) * (zOffset * fullSizeY * fullSizeX + yOffset * fullSizeX + xOffset), SEEK_CUR);
    int uind = 0;
    
    for(int k=0; k<gridSizeZ; k++)
    {
        for(int j=0; j<gridSizeY; j++)
        {
            fwrite(u + uind, sizeof(double), gridSizeX, outfile);
            uind += gridSizeX;
            fseek(outfile, sizeof(double) * (fullSizeX - gridSizeX), SEEK_CUR);
        }
        
        fseek(outfile, sizeof(double) * (fullSizeY - gridSizeY) * fullSizeX, SEEK_CUR);
    }
    
    fclose(outfile);
}//end WriteOutput function

static long fsize(const char* file)
{
    FILE * f = fopen(file, "rb");
    fseek(f, 0, SEEK_END);
    long len = ftell(f);
    fclose(f);
    return len;
}
