long SetupVTUAsync(double *Coords, int *Conn, const char * filename,
        int Nx, int Ny, int Nz);

void WriteVTUAsync(int xOffset, int yOffset, int zOffset, int gridSizeX, int gridSizeY, int gridSizeZ, 
        int nx, int ny, int nz, double* u, char* name, long fileOffset);