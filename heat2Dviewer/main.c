/* 
 * File:   main.c
 * Author: quadmium
 *
 * Created on June 26, 2016, 12:29 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <SDL.h>
#include <SDL_ttf.h>
#include <time.h>
#include <math.h>
#include "main.h"

SDL_Rect windowTarget = {.x = 0, .y = 0, .w = 600, .h = 600};
SDL_Rect pixelTarget;
int meshSize;
unsigned long long simSteps;
double diffusion;
double deltaT;
Uint32* pixels;
double* data;

int main(int argc, char ** argv)
{
    FILE* dataFile = fopen("0.hsim", "r");
    if(dataFile==NULL)
        return 0;
    
    fread(&meshSize, sizeof(int), 1, dataFile);
    fread(&simSteps, sizeof(unsigned long long), 1, dataFile);
    fread(&diffusion, sizeof(double), 1, dataFile);
    fread(&deltaT, sizeof(double), 1, dataFile);
    
    pixelTarget.x = 0;
    pixelTarget.y = 0;
    pixelTarget.w = meshSize;
    pixelTarget.h = meshSize;
    
    bool quit = false;
    SDL_Event event;

    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init(); 
        
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear"); //nearest

    SDL_Window * window = SDL_CreateWindow("Heat Simulation",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowTarget.w, windowTarget.h, 0);

    SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);
    SDL_Texture * texture = SDL_CreateTexture(renderer,
        SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STATIC, meshSize, meshSize);
    
    TTF_Font *dispFont = TTF_OpenFont("default.ttf",20);
    
    pixels = malloc(sizeof(Uint32) * meshSize * meshSize);
    data = malloc(sizeof(double) * meshSize * meshSize);
    
    bool lastWindowBug = false;
    int lastWindowBugCtr = 0;
    int mouseX=0, mouseY=0;
    bool autoPlay = true;
    bool leftBracketDown = false;
    bool rightBracketDown = false;
    
    while (!quit)
    {
        bool updateScreen = false;
        
        int byteCount = fread(data, sizeof(double), meshSize * meshSize, dataFile);
        updateScreen = byteCount == meshSize * meshSize;
        if(!autoPlay)
            fseek(dataFile, -sizeof(double)*meshSize*meshSize, SEEK_CUR);
        
        if(updateScreen)
            for(int i=0; i<meshSize; i++)
                for(int j=0; j<meshSize; j++)
                    pixels[j*meshSize + i] = RGBA2INT(data[j*meshSize + i], 0, 0, 255);
        
        // A break is missing in the code for SDL leading to mouse set to 0 if left window
        // Documented: https://github.com/openfl/lime/commit/068919bc4c16dcda274a6315d164dd6897fbf8c3?diff=unified
        // This works around it by capturing the faulty event and skipping mouse X,Y if it last occurred
        while(SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_MOUSEMOTION:
                    mouseX = event.motion.x;
                    mouseY = event.motion.y;
                    break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym){
                        case SDLK_LEFTBRACKET:
                            leftBracketDown = true;
                            break;
                        case SDLK_RIGHTBRACKET:
                            rightBracketDown = true;
                            break;
                        case SDLK_p:
                            autoPlay = !autoPlay;
                            break;
                    }
                    break;
                case SDL_KEYUP:
                    switch(event.key.keysym.sym){ 
                        case SDLK_LEFTBRACKET:
                            leftBracketDown = false;
                            break;
                        case SDLK_RIGHTBRACKET:
                            rightBracketDown = false;
                            break;
                    }
                    break;
                case SDL_WINDOWEVENT:
                    lastWindowBug = true;
                    lastWindowBugCtr = 1;
                    break;
                case SDL_QUIT:
                    quit = true;
                    break;
            }
            
            if(lastWindowBugCtr == 1)
                lastWindowBugCtr = 2;
            if(lastWindowBugCtr == 2)
            {
                lastWindowBug = false;
                lastWindowBugCtr = 0;
            }
        }
        
        SDL_RenderClear(renderer);  
        if(updateScreen)
            SDL_UpdateTexture(texture, NULL, pixels, meshSize * sizeof(Uint32));
        SDL_RenderCopy(renderer, texture, &pixelTarget, &windowTarget);
        
        if(dispFont != NULL)
        {
            int centerX = mouseX * meshSize / windowTarget.w;
            int centerY = mouseY * meshSize / windowTarget.h;
            SDL_Color White = {255, 255, 255, 255};
            char dispTxt[100];
            snprintf(dispTxt, 100, "(%d,%d) = %.2f",centerX,centerY,data[centerY*meshSize+centerX]);
            SDL_Surface* surfaceMessage = TTF_RenderText_Blended(dispFont, dispTxt, White);
            SDL_Texture* Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);
            int x=4;
            int y=0;
            SDL_Rect Message_rect = {x, y, surfaceMessage->w, surfaceMessage->h};
            SDL_RenderCopy(renderer, Message, NULL, &Message_rect); 
            SDL_DestroyTexture(Message);
            SDL_FreeSurface(surfaceMessage);
        }
        
        SDL_RenderPresent(renderer);
        
        if(leftBracketDown)
            fseek(dataFile, -(autoPlay ? 2 : 1)*sizeof(double)*meshSize*meshSize, SEEK_CUR);
        if(rightBracketDown)
            fseek(dataFile, (autoPlay ? 0 : 1)*sizeof(double)*meshSize*meshSize, SEEK_CUR);
        SDL_Delay(1);
    }

    free(pixels);
    free(data);
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

void clearMesh()
{
    memset(pixels, 0, sizeof(Uint32) * meshSize * meshSize);
    memset(data, 0, sizeof(double) * meshSize * meshSize);
}

Uint32 RGBA2INT(int R, int G, int B, int A)
{
    return SDL_MapRGBA(SDL_AllocFormat(SDL_PIXELFORMAT_RGBA8888), R, G, B, A);
}