/* 
 * File:   main.c
 * Author: quadmium
 *
 * Created on June 26, 2016, 12:29 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <SDL.h>
#include <SDL_ttf.h>
#include <time.h>
#include <math.h>
#include "main.h"

SDL_Rect windowTarget = {.x = 0, .y = 0, .w = 600, .h = 600};
SDL_Rect pixelTarget;
int meshSizeX;
int meshSizeY;
int meshSizeZ;
double meshLengthX;
double meshLengthY;
double meshLengthZ;
unsigned long long simSteps;
unsigned long long savedSteps;
double diffusion;
double deltaT;
double displayDeltaT;
Uint32* pixels;
double* data;

void handleKeys(SDL_Event* event, int* mouseX, int* mouseY, bool* leftBracketDown, 
                bool* rightBracketDown, bool* autoPlay, bool* lastWindowBug, int* lastWindowBugCtr, 
                bool* quit, int* z, int sizeZ, int* format);

int main(int argc, char ** argv)
{
    FILE* dataFile = fopen("Heat3D.hsim", "r");
    if(dataFile==NULL)
        return 0;
    
    fread(&meshSizeX, sizeof(int), 1, dataFile);
    fread(&meshSizeY, sizeof(int), 1, dataFile);
    fread(&meshSizeZ, sizeof(int), 1, dataFile);
    fread(&meshLengthX, sizeof(double), 1, dataFile);
    fread(&meshLengthY, sizeof(double), 1, dataFile);
    fread(&meshLengthZ, sizeof(double), 1, dataFile);
    fread(&simSteps, sizeof(unsigned long long), 1, dataFile);
    fread(&savedSteps, sizeof(unsigned long long), 1, dataFile);
    fread(&diffusion, sizeof(double), 1, dataFile);
    fread(&deltaT, sizeof(double), 1, dataFile);
    displayDeltaT = deltaT * simSteps / savedSteps;
    
    printf("MX:%d,MY:%d,MZ:%d,LX:%f,LY:%f,LZ:%f,NSTEP:%lld,SSTEP:%lld,DIF:%f,DT:%f,DISPDT:%f\n", meshSizeX, meshSizeY, meshSizeZ, meshLengthX,
            meshLengthY, meshLengthZ, simSteps, savedSteps, diffusion, deltaT, displayDeltaT);
    
    pixelTarget.x = 0;
    pixelTarget.y = 0;
    pixelTarget.w = meshSizeX;
    pixelTarget.h = meshSizeY;
    
    bool quit = false;
    SDL_Event event;

    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init(); 
        
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear"); //nearest

    SDL_Window * window = SDL_CreateWindow("Heat Simulation",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowTarget.w, windowTarget.h, 0);

    SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);
    SDL_Texture * texture = SDL_CreateTexture(renderer,
        SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STATIC, meshSizeX, meshSizeY);
    
    TTF_Font *dispFont = TTF_OpenFont("default.ttf",20);
    
    pixels = malloc(sizeof(Uint32) * meshSizeX * meshSizeY);
    data = malloc(sizeof(double) * meshSizeX * meshSizeY * meshSizeZ);
    
    bool lastWindowBug = false;
    int lastWindowBugCtr = 0;
    int mouseX=0, mouseY=0;
    bool autoPlay = true;
    bool leftBracketDown = false;
    bool rightBracketDown = false;
    int alreadyMoved = 0;
    
    int z = meshSizeZ / 2;
    bool justSwitchedZ = false;
    int format = 0;
    
    while (!quit)
    {
        bool updateScreen = false;
        
        int byteCount = fread(data, sizeof(double), meshSizeX * meshSizeY * meshSizeZ, dataFile);
        updateScreen = byteCount == meshSizeX * meshSizeY * meshSizeZ;
        if(!autoPlay && !justSwitchedZ)
            fseek(dataFile, -sizeof(double) * meshSizeX * meshSizeY * meshSizeZ, SEEK_CUR);
        
        int oldz = z;
        handleKeys(&event, &mouseX, &mouseY, &leftBracketDown, 
                &rightBracketDown, &autoPlay, &lastWindowBug, &lastWindowBugCtr, 
                &quit, &z, meshSizeZ, &format);
        if(z != oldz)
        {
            fseek(dataFile, -sizeof(double) * meshSizeX * meshSizeY * meshSizeZ, SEEK_CUR);
            justSwitchedZ = true;
        } else
            justSwitchedZ = false;
        
        // Invert Y such that (0,0) is in bottom left corner
        if(updateScreen)
            for(int i=0; i<meshSizeX; i++)
                for(int j=0; j<meshSizeY; j++)
                    pixels[(meshSizeY-1-j)*meshSizeX + i] = RGBA2INT(255*data[z*meshSizeX*meshSizeY + j*meshSizeX + i], 0, 0, 255);
        
        SDL_RenderClear(renderer);  
        if(updateScreen)
            SDL_UpdateTexture(texture, NULL, pixels, meshSizeX * sizeof(Uint32));
        SDL_RenderCopy(renderer, texture, &pixelTarget, &windowTarget);
        
        if(dispFont != NULL)
        {
            int centerX = mouseX * meshSizeX / windowTarget.w;
            int centerY = mouseY * meshSizeY / windowTarget.h;
            SDL_Color White = {255, 255, 255, 255};
            char dispTxt[100];
            if(format == 0)
                snprintf(dispTxt, 100, "(%.02f,%.02f,%.02f) = %.02f",
                        centerX*meshLengthX/meshSizeX,
                        centerY*meshLengthY/meshSizeY,
                        z*meshLengthZ/meshSizeZ,data[z*meshSizeX*meshSizeY + centerY*meshSizeX+centerX]);
            else if (format == 1)
                snprintf(dispTxt, 100, "(%d,%d,%d) = %.02f",
                        centerX,
                        centerY,
                        z,data[z*meshSizeX*meshSizeY + centerY*meshSizeX+centerX]);
            SDL_Surface* surfaceMessage = TTF_RenderText_Blended(dispFont, dispTxt, White);
            SDL_Texture* Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);
            int x=4;
            int y=0;
            SDL_Rect Message_rect = {x, y, surfaceMessage->w, surfaceMessage->h};
            SDL_RenderCopy(renderer, Message, NULL, &Message_rect); 
            SDL_DestroyTexture(Message);
            SDL_FreeSurface(surfaceMessage);
        }
        
        SDL_RenderPresent(renderer);
        
        if(leftBracketDown)
            fseek(dataFile, -(autoPlay ? 2 : !alreadyMoved)*sizeof(double)*meshSizeX*meshSizeY*meshSizeZ, SEEK_CUR);
        else if(rightBracketDown)
            fseek(dataFile, (autoPlay ? 0 : !alreadyMoved)*sizeof(double)*meshSizeX*meshSizeY*meshSizeZ, SEEK_CUR);
        
        if(leftBracketDown || rightBracketDown)
            alreadyMoved = 1;
        else
            alreadyMoved = 0;
        
        SDL_Delay(displayDeltaT * 1000);
    }

    free(pixels);
    free(data);
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

void handleKeys(SDL_Event* event, int* mouseX, int* mouseY, bool* leftBracketDown, 
                bool* rightBracketDown, bool* autoPlay, bool* lastWindowBug, int* lastWindowBugCtr, 
                bool* quit, int* z, int sizeZ, int* format)
{
    // A break is missing in the code for SDL leading to mouse set to 0 if left window
    // Documented: https://github.com/openfl/lime/commit/068919bc4c16dcda274a6315d164dd6897fbf8c3?diff=unified
    // This works around it by capturing the faulty event and skipping mouse X,Y if it last occurred
    while(SDL_PollEvent(event))
    {
        switch (event->type)
        {
            case SDL_MOUSEMOTION:
                *mouseX = event->motion.x;
                *mouseY = event->motion.y;
                break;
            case SDL_KEYDOWN:
                switch(event->key.keysym.sym){
                    case SDLK_LEFTBRACKET:
                        *leftBracketDown = true;
                        break;
                    case SDLK_RIGHTBRACKET:
                        *rightBracketDown = true;
                        break;
                    case SDLK_p:
                        *autoPlay = !*autoPlay;
                        break;
                    case SDLK_w:
                        (*z)++;
                        if(*z<0) *z = 0;
                        if(*z>=sizeZ) *z = sizeZ - 1;
                        break;
                    case SDLK_s:
                        (*z)--;
                        if(*z<0) *z = 0;
                        if(*z>=sizeZ) *z = sizeZ - 1;
                        break;
                    case SDLK_f:
                        if(!event->key.repeat)
                            (*format)++;
                            if(*format > 1) *format = 0;
                        break;
                }
                break;
            case SDL_KEYUP:
                switch(event->key.keysym.sym){ 
                    case SDLK_LEFTBRACKET:
                        *leftBracketDown = false;
                        break;
                    case SDLK_RIGHTBRACKET:
                        *rightBracketDown = false;
                        break;
                }
                break;
            case SDL_WINDOWEVENT:
                *lastWindowBug = true;
                *lastWindowBugCtr = 1;
                break;
            case SDL_QUIT:
                *quit = true;
                break;
        }

        if(*lastWindowBugCtr == 1)
            *lastWindowBugCtr = 2;
        if(*lastWindowBugCtr == 2)
        {
            *lastWindowBug = false;
            *lastWindowBugCtr = 0;
        }
    }
}

void clearMesh()
{
    memset(pixels, 0, sizeof(Uint32) * meshSizeX * meshSizeY);
    memset(data, 0, sizeof(double) * meshSizeX * meshSizeY * meshSizeZ);
}

Uint32 RGBA2INT(int R, int G, int B, int A)
{
    return SDL_MapRGBA(SDL_AllocFormat(SDL_PIXELFORMAT_RGBA8888), R, G, B, A);
}