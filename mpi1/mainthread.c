#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <mpi.h> 
#include <SDL.h>
#include <SDL_ttf.h>
#include <time.h>
#include <math.h>
#include <limits.h> 
#include "mainthread.h"

int meshSize = 200; // 600 bench
int debug = 1;
SDL_Rect windowTarget = {.x = 0, .y = 0, .w = 600, .h = 600};
SDL_Rect pixelTarget;
double diffusion = 1;
Uint32 * pixels;
double * u;
double cursorSize = 3;
int animate = 0;
double timeA = 0;
double animData = 0;
char* pictureQuality = "nearest"; //nearest, linear, or best

bool lastWindowBug = false;
int lastWindowBugCtr = 0;
bool leftMouseButtonDown = false;
bool rightMouseButtonDown = false;
int bracketDown = 0;
bool quit = false;
SDL_Event event;
int mouseX=0, mouseY=0;

SDL_Renderer* renderer;
SDL_Texture* texture;
int rectOffset=5;
bool dispRect = false;
TTF_Font* dispFont;

MPI_Request* comm;
int size;
int rank;

void mainThread(int rankX, int sizeX)
{
    size=sizeX;
    rank=rankX;
    int initProperties[2];
    initProperties[0] = meshSize;
    initProperties[1] = debug;
    double floatProperties[1];
    floatProperties[0] = diffusion;
    MPI_Bcast(&initProperties, 2, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&floatProperties, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    pixelTarget.x = 0;
    pixelTarget.y = 0;
    pixelTarget.w = meshSize;
    pixelTarget.h = meshSize;

    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init(); 
        
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, pictureQuality); 

    SDL_Window * window = SDL_CreateWindow("Heat Simulation",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowTarget.w, windowTarget.h, 0);

    renderer = SDL_CreateRenderer(window, -1, 0);
    texture = SDL_CreateTexture(renderer,
        SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STATIC, meshSize, meshSize);
    
    dispFont = TTF_OpenFont("default.ttf",20);
    
    pixels = malloc(sizeof(Uint32) * meshSize * meshSize);
    u = malloc(sizeof(double) * meshSize * meshSize);
    setup2();
    
    MPI_Request temp[size-1];
    comm = temp;
    
    // Send the data for processing
    int base = meshSize / (size - 1);
    int leftOver = meshSize % (size - 1);
    int bufIndex = 0;
    for(int targetRank=1; targetRank<size; targetRank++)
    {
        int sendN = base + (leftOver >= targetRank);
        MPI_Send(u + bufIndex, meshSize * sendN, MPI_DOUBLE, targetRank, targetRank, MPI_COMM_WORLD);
        MPI_Recv_init(u + bufIndex, meshSize * sendN, MPI_DOUBLE, targetRank, targetRank, MPI_COMM_WORLD, comm + (targetRank-1));
        printf("0: Sent %d from data + %d to %d\n", meshSize * sendN, bufIndex, targetRank);
        bufIndex += meshSize * sendN;
    }
    
    time_t start = time(NULL);
    bool reachedTarget = false;
    while (!quit)
    {
        SDL_UpdateTexture(texture, NULL, pixels, meshSize * sizeof(Uint32));
        
        handleEvents();
        updateMesh();
        renderScreen();

        if(!reachedTarget && u[meshSize * meshSize / 2 + meshSize / 2] < 80.0)
        {
            time_t end = time(NULL);
            float seconds = (float)(end - start);// / CLOCKS_PER_SEC;
            printf("0: Reached benchmark at t: %f sec\n", seconds);
            reachedTarget = true;
        }
        //timeA += 0.016;
        //SDL_Delay(16);
    }

    free(pixels);
    free(u);
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    
    MPI_Abort(MPI_COMM_WORLD, 1);
}

void updateMesh()
{
    //if(animate != 0)
    //    animateGrid();
    MPI_Startall(size-1, comm);
    MPI_Waitall(size-1, comm, MPI_STATUSES_IGNORE);
    for(int i=0; i<meshSize; i++)
        for(int j=0; j<meshSize; j++)
            pixels[j*meshSize + i] = RGBA2INT(u[j*meshSize + i],0,0,255);
}

void renderScreen()
{
    SDL_RenderClear(renderer);  
    SDL_RenderCopy(renderer, texture, &pixelTarget, &windowTarget);
    if(dispRect)
        genRectangles();

    if(dispFont != NULL)
    {
        int centerX = mouseX * meshSize / windowTarget.w;
        int centerY = mouseY * meshSize / windowTarget.h;
        SDL_Color White = {255, 255, 255, 255};
        char dispTxt[100];
        snprintf(dispTxt, 100, "(%d,%d) = %.2f",centerX,centerY,u[centerY*meshSize+centerX]);
        SDL_Surface* surfaceMessage = TTF_RenderText_Blended(dispFont, dispTxt, White);
        SDL_Texture* Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);
        int x=4;
        int y=0;
        SDL_Rect Message_rect = {x, y, surfaceMessage->w, surfaceMessage->h};
        SDL_RenderCopy(renderer, Message, NULL, &Message_rect); 
        SDL_DestroyTexture(Message);
        SDL_FreeSurface(surfaceMessage);
    }

    SDL_RenderPresent(renderer);
}

void handleEvents()
{
    // A break is missing in the code for SDL leading to mouse set to 0 if left window
    // Documented: https://github.com/openfl/lime/commit/068919bc4c16dcda274a6315d164dd6897fbf8c3?diff=unified
    // This works around it by capturing the faulty event and skipping mouse X,Y if it last occurred
    while(SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_MOUSEBUTTONUP:
                if (event.button.button == SDL_BUTTON_LEFT)
                    leftMouseButtonDown = false;
                else if (event.button.button == SDL_BUTTON_RIGHT)
                    rightMouseButtonDown = false;
                break;
            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button == SDL_BUTTON_LEFT)
                    leftMouseButtonDown = true;
                else if (event.button.button == SDL_BUTTON_RIGHT)
                    rightMouseButtonDown = true;
                break;
            case SDL_MOUSEMOTION:
                mouseX = event.motion.x;
                mouseY = event.motion.y;
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym){
                    case SDLK_1:
                        setup1();
                        break;
                    case SDLK_2:
                        setup2();
                        break;
                    case SDLK_3:
                        setup3();
                        break;
                    case SDLK_4:
                        setup4();
                        break;
                    case SDLK_5:
                        setup5();
                        break;
                    case SDLK_6:
                        setup6();
                        break;
                    case SDLK_7:
                        setup7();
                        break;
                    case SDLK_LEFTBRACKET:
                        bracketDown = -1;
                        break;
                    case SDLK_RIGHTBRACKET:
                        bracketDown = 1;
                        break;
                    case SDLK_c:
                        rectOffset++;
                        break;
                    case SDLK_v:
                        dispRect = !dispRect;
                        break;
                }
                break;
            case SDL_KEYUP:
                switch(event.key.keysym.sym){
                    case SDLK_LEFTBRACKET:
                        bracketDown = 0;
                        break;
                    case SDLK_RIGHTBRACKET:
                        bracketDown = 0;
                        break;
                }
                break;
            case SDL_WINDOWEVENT:
                lastWindowBug = true;
                lastWindowBugCtr = 1;
                break;
            case SDL_QUIT:
                quit = true;
                break;
        }

        if(bracketDown != 0)
            cursorSize += 0.016 * bracketDown;

        if (!lastWindowBug && (leftMouseButtonDown || rightMouseButtonDown))
        {
            mouseX = event.motion.x;
            mouseY = event.motion.y;
            int centerX = mouseX * meshSize / windowTarget.w;
            int centerY = mouseY * meshSize / windowTarget.h;
            double set = leftMouseButtonDown ? 255.0 : 0;
            for(int i = centerX-cursorSize; i < centerX + cursorSize; i++)
                for(int j = centerY-cursorSize; j < centerY + cursorSize; j++)
                    if((i-centerX)*(i-centerX) + (j-centerY)*(j-centerY) <= cursorSize*cursorSize)
                        setPoint(i,j,set);
        }

        if(lastWindowBugCtr == 1)
            lastWindowBugCtr = 2;
        if(lastWindowBugCtr == 2)
        {
            lastWindowBug = false;
            lastWindowBugCtr = 0;
        }
    }
}

void setup1()
{
    clearMesh();
    animate = 0;
    
    for(int i = 0; i < meshSize; i++)
    {
        setPoint(0,meshSize-i/2-1,255.0);
        setPoint(meshSize-1,meshSize-i/2-1,255.0);
        setPoint(i,meshSize-1,255.0);
    }
}

void setup2()
{
    clearMesh();
    animate = 0;
    
    for(int i=1; i<meshSize-1; i++)
        for(int j = meshSize/2-5; j < meshSize/2+5; j++)
            {
                setPoint(i,j,255.0);
                setPoint(j,i,255.0);
            }
}

void setup3()
{
    clearMesh();
    animate = 0;
    
    for(int i=1; i<meshSize-1; i++)
        for(int j=1; j<meshSize-1; j++)
            setPoint(i,j,255-255*sqrt(2)/meshSize*sqrt((i-meshSize/2)*(i-meshSize/2) + (j-meshSize/2)*(j-meshSize/2)));
}

void setup4()
{
    clearMesh();
    animate = 0;
    
    for(int i=1; i<meshSize-1; i++)
        for(int j=1; j<meshSize-1; j++)
            setPoint(i,j,255.0 / 2 * abs(sin(0.2*60/meshSize*i) + sin(0.2*60/meshSize*j)));
}

void setup5()
{
    clearMesh();
    animate = 1;
    timeA = 0;
    
    for(int i = 1; i < meshSize-1; i++)
        setPoint(meshSize/2,i,255.0);
}

void setup6()
{
    clearMesh();
    animate = 2;
    timeA = 0;
    animData = 255.0;
}

void setup7()
{
    clearMesh();
    animate = 3;
    timeA = 0;
}

void animateGrid()
{
    double period;
    switch(animate)
    {
        case 1:
            period = 6;
            for(int i = 1; i < meshSize-1; i++)
                setPoint((int)(meshSize/2 + (meshSize-2)/2.0*sin(2*M_PI/period*timeA)),i,255.0);
            if(timeA > period) timeA -= period;
            break;
        case 2:
            period = 7;
            anim6Helper(period, timeA, animData);
            if(timeA > period)
            {
                timeA=0;
                animData = 255.0-animData;
            }
            break;
        case 3:
            period = 7;
            int numDivisions = 8;
            for(int i=0; i<numDivisions; i++)
                anim6Helper(period, timeA - i*period/numDivisions,i%2==0 ? 255.0 : 0);
            if(timeA > period) timeA = 0;
            break;
    }
}

void anim6Helper(double period, double t, double val)
{
    while(t<0) t += period;
    if(t <= period/4)
        setPoint((int)((meshSize-1)*4*t/period), 0, val);
    else if (t <= period / 2)
        setPoint(meshSize-1,(int)((meshSize-1)*4*(t-period/4)/period),val);
    else if (t <= 3 * period / 4)
        setPoint(meshSize-1-(int)((meshSize-1)*4*(t-period/2)/period),meshSize-1,val);
    else if (t <= period)
        setPoint(0,meshSize-1-(int)((meshSize-1)*4*(t-3*period/4)/period),val);
}

void clearMesh()
{
    memset(pixels, 0, sizeof(Uint32) * meshSize * meshSize);
    memset(u, 0, sizeof(double) * meshSize * meshSize);
}

Uint32 RGBA2INT(int R, int G, int B, int A)
{
    return SDL_MapRGBA(SDL_AllocFormat(SDL_PIXELFORMAT_RGBA8888), R, G, B, A);
}

void setPoint(int x, int y, double val)
{
    if(x < 0 || y < 0 || x >= meshSize || y >= meshSize) return;
    
    if(val > 255) val = 255;
    if(val < 0) val = 0;

    pixels[y * meshSize + x] = RGBA2INT(val,0,0,255);
    u[y * meshSize + x] = val;
}

unsigned int hash(unsigned int x)
{
    x = ((x >> 16) ^ x) * 0x45d9f3b;
    x = ((x >> 16) ^ x) * 0x45d9f3b;
    x = ((x >> 16) ^ x);
    return x;
}

void genRectangles()
{
    int totalHeight = 0;
    for(int i=1; i<size; i++)
    {
        SDL_SetRenderDrawColor(renderer, hash(i+rectOffset)%255, hash(i+100+rectOffset)%255, hash(i+200+rectOffset)%255, 0.2);
        int leftOver = windowTarget.h % (size-1);
        int height = windowTarget.h / (size-1) + (i <= leftOver);
        SDL_Rect rect = {.x = 0, .y = totalHeight, .w = windowTarget.w, .h = height};
        totalHeight += height;
        SDL_RenderDrawRect(renderer, &rect);
    }
}