#include <SDL.h>

void mainThread(int rank, int size);
void handleEvents();
void renderScreen();
void updateMesh();
Uint32 RGBA2INT(int R, int G, int B, int A);
void setPoint();
void setup1();
void setup2();
void setup3();
void setup4();
void setup5();
void setup6();
void setup7();
void anim6Helper();
void animateGrid();
void clearMesh();
unsigned int hash(unsigned int x);
void genRectangles();