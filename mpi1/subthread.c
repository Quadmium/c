#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <mpi.h>
#include <string.h> 
#include <limits.h> 
#include "subthread.h"
#ifdef WIN32
#include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
#include <time.h>   // for nanosleep
#else
#include <unistd.h> // for usleep
#endif

void sleep_ms(int milliseconds) // cross-platform sleep function
{
#ifdef WIN32
    Sleep(milliseconds);
#elif _POSIX_C_SOURCE >= 199309L
    struct timespec ts;
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
#else
    usleep(milliseconds * 1000);
#endif
}

int meshSize;
int debug;
double diffusion;
int localN;
int sSize; // Size of subthreads
double* u[2];
double* uSendBuf;
int arSize;
int garbage; // Used for unnecessary results such as flag for termination
MPI_Request mainReq;

void subThread(int rank, int size)
{
    sSize = size - 1;
    
    int initProperties[2];
    double floatProperties[1];
    // Receive initial conditions
    MPI_Bcast(&initProperties, 2, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&floatProperties, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    meshSize = initProperties[0];
    debug = initProperties[1];
    diffusion = floatProperties[0];
    
    // Use rank to determine portion of grid (top to bottom)
    int base = meshSize / sSize;
    int leftOver = meshSize % sSize;
    localN = base + (rank > 1) + (rank < sSize) + (leftOver >= rank);
    int recvN = base + (leftOver >= rank);
    
    arSize = meshSize * localN;
    u[0] = malloc(sizeof(double) * arSize);
    u[1] = malloc(sizeof(double) * arSize);
    uSendBuf = malloc(sizeof(double) * meshSize * recvN);
    
    if(debug) printf("%d: Started, meshSize: %d, arraySize: %d\n", rank, meshSize, arSize);
    
    // First should insert at start (no boundary), following should insert after the first border
    int insertIndex = (rank != 1) * meshSize;
    MPI_Recv(u[0] + insertIndex, meshSize * recvN, MPI_DOUBLE, 0, rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    if(debug) printf("%d: Received %d at data + %d\n", rank, meshSize * recvN, insertIndex);
    memcpy(u[1], u[0], sizeof(double) * arSize);
    int quit = 0;
    MPI_Request quitRequest;
    MPI_Irecv(&quit, 1, MPI_INT, 0, INT_MAX, MPI_COMM_WORLD, &quitRequest);
    
    // u contains two copies so there's no need to copy unew to uold after loop, just switch index
    int newi = 1;
    int oldi = 0;
    int tag = 0; // Alternate tag between timesteps
    int step = 0;
    int stepMod = 1;
    // Sending only when step % stepMod = 0 means every processor will only send after 
    // stepMod timesteps. Used for benchmarking such that the screen is updated slower meaning
    // more emphasis on internal computation than transportation of data to the GUI.
    // Setting stepMod to 1 will send as fast as possible showing effects of lots of sending
    
    MPI_Request topBoundSend, topBoundReceive, botBoundSend, botBoundReceive;
    while(!quit)
    {
        if(rank > 1) // Top bounds
        {
            // Send thread above me the second row (i have it and updated it last step)
            // Receive from thread above me the first row (i can't update it but they did last step)
            MPI_Isend(u[oldi] + meshSize, meshSize, MPI_DOUBLE, rank-1, tag, MPI_COMM_WORLD, &topBoundSend);
            MPI_Irecv(u[oldi], meshSize, MPI_DOUBLE, rank-1, tag, MPI_COMM_WORLD, &topBoundReceive);
        }
        
        if(rank < sSize) // Bottom Bounds
        {
            // Send thread below me the last - 1 row (i have it and updated it last step)
            // Receive from thread below me the last row (i can't update it but they did last step)
            MPI_Isend(u[oldi] + meshSize * (localN - 2), meshSize, MPI_DOUBLE, rank+1, tag, MPI_COMM_WORLD, &botBoundSend);
            MPI_Irecv(u[oldi] + meshSize * (localN - 1), meshSize, MPI_DOUBLE, rank+1, tag, MPI_COMM_WORLD, &botBoundReceive);
        }
        
        int startY = rank == 1 ? 1 : 2;
        int endY = rank == sSize ? localN - 1 : localN - 2;
        
        // Update everything except bounds (unless at first or last index since no data needs to exchange)
        for(int x = 1; x < meshSize-1; x++)
            for(int y = startY; y < endY; y++)
                updatePoint(x, y, oldi, newi);
        
        // End checking and updating
        // Need to check the 'send' request in order to close it
        // http://www.clustermonkey.net/MPI/mpi-the-top-ten-mistakes-to-avoid-part-1.html
        int ready[4] = {0, 0, 0, 0};
        bool updated[2] = {false, false};
        if(rank == 1)
        {
            ready[0] = 1;
            ready[1] = 1;
        }
        if(rank == sSize)
        {
            ready[2] = 1;
            ready[3] = 1;
        }
        
        while(!updated[0] || !updated[1])
        {
            if(!updated[0])
            {
                if(!ready[0])
                    MPI_Test(&topBoundReceive, ready, MPI_STATUS_IGNORE);
                if(!ready[1])
                    MPI_Test(&topBoundSend, ready + 1, MPI_STATUS_IGNORE);
            }
            
            if(!updated[1])
            {
                if(!ready[2])
                    MPI_Test(&botBoundReceive, ready + 2, MPI_STATUS_IGNORE);
                if(!ready[3])
                    MPI_Test(&botBoundSend, ready + 3, MPI_STATUS_IGNORE);
            }
            
            if(ready[0] && ready[1] && !updated[0])
            {
                for(int x=1; x < meshSize-1; x++)
                    updatePoint(x, 1, oldi, newi);
                updated[0] = true;
            }
            
            if(ready[2] && ready[3] && !updated[1])
            {
                for(int x=1; x < meshSize-1; x++)
                    updatePoint(x, localN-2, oldi, newi);
                updated[1] = true;
            }
        }
        
        
        int mainFlag = 1;
        if(mainReq != NULL)
            MPI_Test(&mainReq, &mainFlag, MPI_STATUS_IGNORE);
            
        if(mainFlag == 1 && step % stepMod == 0)
        {
            memcpy(uSendBuf, u[newi] + insertIndex, sizeof(double) * meshSize * recvN);
            MPI_Isend(uSendBuf, meshSize * recvN, MPI_DOUBLE, 0, rank, MPI_COMM_WORLD, &mainReq);
        }
        
        newi = 1-newi;
        oldi = 1-oldi;
        tag = 1-tag;
        step++;
        sleep_ms(16);
        MPI_Test(&quitRequest, &garbage, MPI_STATUS_IGNORE);
    }
    
    if(debug) printf("%d: Quitting\n", rank);
}

void updatePoint(int x, int y, int oldi, int newi)
{
    // b = before, a = after
    // finite difference in 2D heat equation leads to delta shown
    double bx = u[oldi][y*meshSize + (x-1)];
    double ax = u[oldi][y*meshSize + (x+1)];
    double by = u[oldi][(y-1)*meshSize + x];
    double ay = u[oldi][(y+1)*meshSize + x];
    double c = u[oldi][y*meshSize + x];
    double delta = 0.16 * diffusion * (bx+ax+by+ay-4*c);

    u[newi][y*meshSize + x] = u[oldi][y*meshSize + x] + delta;
}