/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: quadmium
 *
 * Created on June 26, 2016, 12:29 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h> 
/*
 * 
 */
int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    int data;
    if(rank > 0)
        MPI_Recv(&data, 1, MPI_INT, rank-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    
    data = 2;
    if(rank < size-1)
        MPI_Send(&data, 1, MPI_INT, rank+1, 0, MPI_COMM_WORLD);
    
    printf("Hello I am %d out of %d\n", rank, size);
    
    MPI_Finalize();
    return (EXIT_SUCCESS);
}