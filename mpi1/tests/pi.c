/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: quadmium
 *
 * Created on June 26, 2016, 12:29 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h> 

void throwDarts();

int numDarts = 10000000;
int inside = 0;

int rank;
int meshSize;

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &meshSize);
    
    throwDarts();
    
    double totalPercent;
    double myPercent = ((double)inside)/numDarts;
    MPI_Reduce(&myPercent, &totalPercent, 1, MPI_DOUBLE, MPI_SUM, 0,
               MPI_COMM_WORLD);
    if(rank == 0)
    {
        printf("%f", 4*totalPercent/meshSize);
    }
    
    MPI_Finalize();
    return (EXIT_SUCCESS);
}

void throwDarts()
{
    double div = RAND_MAX;
    srand(rank);
    for(int i=0; i<numDarts; i++)
    {
        double x = rand() / div;
        double y = rand() / div;
        if(x*x + y*y <= 1)
            inside++;
    }
}