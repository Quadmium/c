/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: quadmium
 *
 * Created on June 26, 2016, 12:29 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h> 
#include "subthread.h"

int rank;
int size;

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    //if(rank == 0)
    //    mainThread(rank, size);
    //else
    subThread(rank+1, size+1);
    
    MPI_Finalize();
    return (EXIT_SUCCESS);
}