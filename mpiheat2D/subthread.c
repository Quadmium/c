#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <mpi.h>
#include <string.h> 
#include <limits.h> 
#include <math.h> 
#include "subthread.h"

int debug;
int startRow;
int recvN;
int meshSize = 100;
unsigned long long simSteps = (unsigned long long)(400/0.16);
unsigned long long step = 0;
int debug = 1;
double diffusion = 1;
double deltaT = 0.16;
int localN;
int sSize; // Size of subthreads
double* u[2];
double* uStart;
int arSize;
int garbage; // Used for unnecessary results such as flag for termination
MPI_Request mainReq;

void updateAnimation(int writei);
void setData(int x, int y, double value, int writei);
void setStartPoint(int x, int y, double val);

void subThread(int rank, int size)
{
    sSize = size - 1;
    long headerOffset;
    if(rank==1)
    {
        FILE* output = fopen("out/0.hsim", "wb");
        fwrite(&meshSize, sizeof(int), 1, output);
        fwrite(&simSteps, sizeof(unsigned long long), 1, output);
        fwrite(&diffusion, sizeof(double), 1, output);
        fwrite(&deltaT, sizeof(double), 1, output);
        headerOffset = ftell(output);
        fclose(output);
        MPI_Bcast(&headerOffset, 1, MPI_LONG, 0, MPI_COMM_WORLD);
        
    }
    MPI_Bcast(&headerOffset, 1, MPI_LONG, 0, MPI_COMM_WORLD);
    
    uStart = malloc(sizeof(double) * meshSize * meshSize);
    memset(uStart, 0, sizeof(double) * meshSize * meshSize);
    for(int i=1; i<meshSize-1; i++)
    for(int j = meshSize/2-5; j < meshSize/2+5; j++)
        {
            setStartPoint(i,j,255.0);
            setStartPoint(j,i,255.0);
        }
    // Use rank to determine portion of grid (top to bottom)
    int base = meshSize / sSize;
    int leftOver = meshSize % sSize;
    localN = base + (rank > 1) + (rank < sSize) + (leftOver >= rank);
    recvN = base + (leftOver >= rank);
    
    arSize = meshSize * localN;
    u[0] = malloc(sizeof(double) * arSize);
    u[1] = malloc(sizeof(double) * arSize);
    startRow = base * (rank-1) + (leftOver >= rank-1 ? rank-1 : leftOver);
    long startInd = meshSize * startRow;
    
    if(debug) printf("%d: Started, meshSize: %d, arraySize: %d, simSteps: %llu, diffusion: %f, deltaT: %f\n", rank, meshSize, arSize, simSteps, diffusion, deltaT);
    
    // First should insert at start (no boundary), following should insert after the first border
    int insertIndex = (rank != 1) * meshSize;
    memcpy(u[0] + insertIndex, uStart + startInd, sizeof(double) * meshSize * recvN);
    //MPI_Recv(u[0] + insertIndex, meshSize * recvN, MPI_DOUBLE, 0, rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    //if(debug) printf("%d: Received %d at data + %d\n", rank, meshSize * recvN, insertIndex);
    memcpy(u[1], u[0], sizeof(double) * arSize);
    
    FILE* output = fopen("out/0.hsim", "r+b");
    /*  leftover = 2
        1: 0
        2: 0 + len(1) = 0 + base + 1
        3: 0 + base + 1 + base + 1
        4: 0 + base + 1 + base + 1 + base
    */
    fseek(output, headerOffset, SEEK_SET);
    if(debug) printf("%d: Writing at row: %ld, index: %ld\n", rank, startInd / meshSize, startInd);
    startInd *= sizeof(double);
    fseek(output, startInd, SEEK_CUR);
    
    // u contains two copies so there's no need to copy unew to uold after loop, just switch index
    int newi = 1;
    int oldi = 0;
    int tag = 0; // Alternate tag between timesteps
    step = 0;
    
    MPI_Request topBoundSend, topBoundReceive, botBoundSend, botBoundReceive;
    while(step < simSteps)
    {
        if(rank > 1) // Top bounds
        {
            // Send thread above me the second row (i have it and updated it last step)
            // Receive from thread above me the first row (i can't update it but they did last step)
            MPI_Isend(u[oldi] + meshSize, meshSize, MPI_DOUBLE, rank-1-1, tag, MPI_COMM_WORLD, &topBoundSend);
            MPI_Irecv(u[oldi], meshSize, MPI_DOUBLE, rank-1-1, tag, MPI_COMM_WORLD, &topBoundReceive);
        }
        
        if(rank < sSize) // Bottom Bounds
        {
            // Send thread below me the last - 1 row (i have it and updated it last step)
            // Receive from thread below me the last row (i can't update it but they did last step)
            MPI_Isend(u[oldi] + meshSize * (localN - 2), meshSize, MPI_DOUBLE, rank+1-1, tag, MPI_COMM_WORLD, &botBoundSend);
            MPI_Irecv(u[oldi] + meshSize * (localN - 1), meshSize, MPI_DOUBLE, rank+1-1, tag, MPI_COMM_WORLD, &botBoundReceive);
        }
        
        int startY = rank == 1 ? 1 : 2;
        int endY = rank == sSize ? localN - 1 : localN - 2;
        
        // Update everything except bounds (unless at first or last index since no data needs to exchange)
        for(int x = 1; x < meshSize-1; x++)
            for(int y = startY; y < endY; y++)
                updatePoint(x, y, oldi, newi);
        
        // End checking and updating
        // Need to check the 'send' request in order to close it
        // http://www.clustermonkey.net/MPI/mpi-the-top-ten-mistakes-to-avoid-part-1.html
        int ready[4] = {0, 0, 0, 0};
        bool updated[2] = {false, false};
        if(rank == 1)
        {
            ready[0] = 1;
            ready[1] = 1;
        }
        if(rank == sSize)
        {
            ready[2] = 1;
            ready[3] = 1;
        }
        
        while(!updated[0] || !updated[1])
        {
            if(!updated[0])
            {
                if(!ready[0])
                    MPI_Test(&topBoundReceive, ready, MPI_STATUS_IGNORE);
                if(!ready[1])
                    MPI_Test(&topBoundSend, ready + 1, MPI_STATUS_IGNORE);
            }
            
            if(!updated[1])
            {
                if(!ready[2])
                    MPI_Test(&botBoundReceive, ready + 2, MPI_STATUS_IGNORE);
                if(!ready[3])
                    MPI_Test(&botBoundSend, ready + 3, MPI_STATUS_IGNORE);
            }
            
            if(ready[0] && ready[1] && !updated[0])
            {
                for(int x=1; x < meshSize-1; x++)
                    updatePoint(x, 1, oldi, newi);
                updated[0] = true;
            }
            
            if(ready[2] && ready[3] && !updated[1])
            {
                for(int x=1; x < meshSize-1; x++)
                    updatePoint(x, localN-2, oldi, newi);
                updated[1] = true;
            }
        }
        
        updateAnimation(newi);
        
        fwrite(u[newi] + insertIndex, sizeof(double), meshSize * recvN, output);
        fseek(output, sizeof(double)*meshSize*(meshSize-recvN), SEEK_CUR);
        newi = 1-newi;
        oldi = 1-oldi;
        tag = 1-tag;
        step++;
    }
    
    if(debug) printf("%d: Quitting, wrote %llu times of %d\n", rank, step, meshSize * recvN);
    
    fclose(output);
}

void updatePoint(int x, int y, int oldi, int newi)
{
    // b = before, a = after
    // finite difference in 2D heat equation leads to delta shown
    double bx = u[oldi][y*meshSize + (x-1)];
    double ax = u[oldi][y*meshSize + (x+1)];
    double by = u[oldi][(y-1)*meshSize + x];
    double ay = u[oldi][(y+1)*meshSize + x];
    double c = u[oldi][y*meshSize + x];
    double delta = deltaT * diffusion * (bx+ax+by+ay-4*c);

    u[newi][y*meshSize + x] = u[oldi][y*meshSize + x] + delta;
}

void updateAnimation(int writei)
{
    double period = 100;
    double timePassed = step * deltaT-period*(int)(step * deltaT / period);
    double xCircle = (meshSize*0.75) / 2 * cos(2*M_PI / period * timePassed);
    double yCircle = (meshSize*0.75) / 2 * sin(2*M_PI / period * timePassed);
    int cursorX = xCircle + meshSize / 2;
    int cursorY = yCircle + meshSize / 2;
    double radius = meshSize/20;
    for(int i = -radius; i <= radius; i++)
        for(int j= -radius; j <= radius; j++)
            if(i*i + j*j <= radius * radius)
                setData(cursorX+i,cursorY+j, 255.0/(1+0.2*sqrt(i*i + j*j)), writei);
    //u[writei][(int)(5*meshSize + (meshSize-5) / 2 * sin(2*M_PI/period*timePassed) + meshSize / 2)] = 255.0;
}

void setData(int x, int y, double value, int writei)
{
    if(y < startRow || y > startRow + recvN) return;
    y -= startRow;
    u[writei][y*meshSize + x] = value;
}

void setStartPoint(int x, int y, double val)
{
    if(x < 0 || y < 0 || x >= meshSize || y >= meshSize) return;
    
    if(val > 255) val = 255;
    if(val < 0) val = 0;

    uStart[y * meshSize + x] = val;
}
