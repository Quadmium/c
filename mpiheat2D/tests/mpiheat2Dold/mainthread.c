#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <mpi.h> 
#include <time.h>
#include <math.h>
#include <limits.h> 
#include <sys/stat.h>
#include "mainthread.h"

int meshSize = 100;
unsigned long long simSteps = (unsigned long long)(400/0.16);
int debug = 1;
double diffusion = 1;
double deltaT = 0.16;
double* u;
int size;
int rank;

void mainThread(int rankX, int sizeX)
{
    size=sizeX;
    rank=rankX;
    
    mkdir("out", 0777);
    FILE* output = fopen("out/0.hsim", "wb");
    fwrite(&meshSize, sizeof(int), 1, output);
    fwrite(&simSteps, sizeof(unsigned long long), 1, output);
    fwrite(&diffusion, sizeof(double), 1, output);
    fwrite(&deltaT, sizeof(double), 1, output);
    long headerOffset = ftell(output);
    fclose(output);
    
    int intProperties[2];
    intProperties[0] = meshSize;
    intProperties[1] = debug;
    double floatProperties[2];
    floatProperties[0] = diffusion;
    floatProperties[1] = deltaT;
    long longProperties[1];
    longProperties[0] = headerOffset;
    
    MPI_Bcast(&intProperties, 2, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&floatProperties, 2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&longProperties, 1, MPI_LONG, 0, MPI_COMM_WORLD);
    MPI_Bcast(&simSteps, 1, MPI_UNSIGNED_LONG_LONG, 0, MPI_COMM_WORLD);
    
    u = malloc(sizeof(double) * meshSize * meshSize);
    memset(u, 0, sizeof(double) * meshSize * meshSize);
    setup2();
    
    // Send the data for processing
    int base = meshSize / (size - 1);
    int leftOver = meshSize % (size - 1);
    int bufIndex = 0;
    for(int targetRank=1; targetRank<size; targetRank++)
    {
        int sendN = base + (leftOver >= targetRank);
        MPI_Send(u + bufIndex, meshSize * sendN, MPI_DOUBLE, targetRank, targetRank, MPI_COMM_WORLD);
        printf("0: Sent %d from data + %d to %d\n", meshSize * sendN, bufIndex, targetRank);
        bufIndex += meshSize * sendN;
    }
    
    free(u);
}

void setup1()
{
    for(int i = 0; i < meshSize; i++)
    {
        setPoint(0,meshSize-i/2-1,255.0);
        setPoint(meshSize-1,meshSize-i/2-1,255.0);
        setPoint(i,meshSize-1,255.0);
    }
}

void setup2()
{
    for(int i=1; i<meshSize-1; i++)
        for(int j = meshSize/2-5; j < meshSize/2+5; j++)
            {
                setPoint(i,j,255.0);
                setPoint(j,i,255.0);
            }
}

void setup3()
{
    for(int i=0; i<meshSize; i++)
        for(int j=0; j<meshSize; j++)
            setPoint(i,j,255-255*sqrt(2)/meshSize*sqrt((i-meshSize/2)*(i-meshSize/2) + (j-meshSize/2)*(j-meshSize/2)));
}

void setup4()
{
    for(int i=1; i<meshSize-1; i++)
        for(int j=1; j<meshSize-1; j++)
            setPoint(i,j,255.0 / 2 * abs(sin(0.2*60/meshSize*i) + sin(0.2*60/meshSize*j)));
}

void setup5()
{
    for(int i = 1; i < meshSize-1; i++)
        setPoint(meshSize/2,i,255.0);
}

void setPoint(int x, int y, double val)
{
    if(x < 0 || y < 0 || x >= meshSize || y >= meshSize) return;
    
    if(val > 255) val = 255;
    if(val < 0) val = 0;

    u[y * meshSize + x] = val;
}