/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: quadmium
 *
 * Created on June 26, 2016, 12:29 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <SDL.h>
#include "main.h"

int size = 30;
SDL_Rect windowTarget = {.x = 0, .y = 0, .w = 640, .h = 480};
SDL_Rect pixelTarget;

int main(int argc, char ** argv)
{
    pixelTarget.x = 0;
    pixelTarget.y = 0;
    pixelTarget.w = size;
    pixelTarget.h = size;
    
    bool leftMouseButtonDown = false;
    bool quit = false;
    SDL_Event event;

    SDL_Init(SDL_INIT_VIDEO);

    SDL_Window * window = SDL_CreateWindow("SDL2 Pixel Drawing",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowTarget.w, windowTarget.h, 0);

    SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);
    SDL_Texture * texture = SDL_CreateTexture(renderer,
        SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STATIC, 640, 480);
    Uint32 * pixels = (Uint32*)malloc(sizeof(*pixels) * size * size);
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, 0);
    
    for(int i=0; i<size * size; i++)
        pixels[i] = RGBA2INT(144,195,212,255);
    
    while (!quit)
    {
        SDL_UpdateTexture(texture, NULL, pixels, size * sizeof(Uint32));
        SDL_WaitEvent(&event);

        switch (event.type)
        {
        case SDL_MOUSEBUTTONUP:
            if (event.button.button == SDL_BUTTON_LEFT)
                leftMouseButtonDown = false;
            break;
        case SDL_MOUSEBUTTONDOWN:
            if (event.button.button == SDL_BUTTON_LEFT)
                leftMouseButtonDown = true;
        case SDL_MOUSEMOTION:
            if (leftMouseButtonDown)
            {
                int mouseX = event.motion.x;
                int mouseY = event.motion.y;
                pixels[mouseY * size / windowTarget.h * size + mouseX * size / windowTarget.w] = 0;
            }
            break;
        case SDL_QUIT:
            quit = true;
            break;
        }

        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, texture, &pixelTarget, &windowTarget);
        SDL_RenderPresent(renderer);
    }

    free(pixels);
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

Uint32 RGBA2INT(int R, int G, int B, int A)
{
    return SDL_MapRGBA(SDL_AllocFormat(SDL_PIXELFORMAT_RGBA8888), R, G, B, A);
}