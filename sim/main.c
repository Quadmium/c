/* 
 * File:   main.c
 * Author: quadmium
 *
 * Created on June 26, 2016, 12:29 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <SDL.h>
#include <SDL_ttf.h>
#include <time.h>
#include <math.h>
#include "main.h"

int size = 100;
SDL_Rect windowTarget = {.x = 0, .y = 0, .w = 600, .h = 600};
SDL_Rect pixelTarget;
double diffusion = 1;
Uint32 * pixels;
double * data[2];
double cursorSize = 3;
int animate = 0;
double timeA = 0;
double animData = 0;
int newi = 1, oldi = 0;

int main(int argc, char ** argv)
{
    pixelTarget.x = 0;
    pixelTarget.y = 0;
    pixelTarget.w = size;
    pixelTarget.h = size;
    
    bool leftMouseButtonDown = false;
    bool rightMouseButtonDown = false;
    int bracketDown = 0;
    bool quit = false;
    SDL_Event event;

    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init(); 
        
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear"); 

    SDL_Window * window = SDL_CreateWindow("Heat Simulation",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowTarget.w, windowTarget.h, 0);

    SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);
    SDL_Texture * texture = SDL_CreateTexture(renderer,
        SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STATIC, size, size);
    
    TTF_Font *dispFont = TTF_OpenFont("default.ttf",20);
    
    pixels = malloc(sizeof(Uint32) * size * size);
    data[0] = malloc(sizeof(double) * size * size);
    data[1] = malloc(sizeof(double) * size * size);
    
    setup1();
    int mouseX=0, mouseY=0;
    
    while (!quit)
    {
        for(int i=1; i<size-1; i++)
        {
            for(int j=1; j<size-1; j++)
            {
                // b = before, a = after
                // finite difference in 2D heat equation leads to delta shown
                double bx = data[oldi][j*size + (i-1)];
                double ax = data[oldi][j*size + (i+1)];
                double by = data[oldi][(j-1)*size + i];
                double ay = data[oldi][(j+1)*size + i];
                double c = data[oldi][j*size + i];
                double delta = 0.16 * diffusion * (bx+ax+by+ay-4*c);
                
                setPoint(i,j,c + delta, false);
            }
        }
        
        if(animate != 0)
            animateGrid();
        
        SDL_UpdateTexture(texture, NULL, pixels, size * sizeof(Uint32));
        bool lastWindowBug = false;
        int lastWindowBugCtr = 0;
        // A break is missing in the code for SDL leading to mouse set to 0 if left window
        // Documented: https://github.com/openfl/lime/commit/068919bc4c16dcda274a6315d164dd6897fbf8c3?diff=unified
        // This works around it by capturing the faulty event and skipping mouse X,Y if it last occurred
        while(SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_MOUSEBUTTONUP:
                    if (event.button.button == SDL_BUTTON_LEFT)
                        leftMouseButtonDown = false;
                    else if (event.button.button == SDL_BUTTON_RIGHT)
                        rightMouseButtonDown = false;
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    if (event.button.button == SDL_BUTTON_LEFT)
                        leftMouseButtonDown = true;
                    else if (event.button.button == SDL_BUTTON_RIGHT)
                        rightMouseButtonDown = true;
                    break;
                case SDL_MOUSEMOTION:
                    mouseX = event.motion.x;
                    mouseY = event.motion.y;
                    break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym){
                        case SDLK_1:
                            setup1();
                            break;
                        case SDLK_2:
                            setup2();
                            break;
                        case SDLK_3:
                            setup3();
                            break;
                        case SDLK_4:
                            setup4();
                            break;
                        case SDLK_5:
                            setup5();
                            break;
                        case SDLK_6:
                            setup6();
                            break;
                        case SDLK_7:
                            setup7();
                            break;
                        case SDLK_LEFTBRACKET:
                            bracketDown = -1;
                            break;
                        case SDLK_RIGHTBRACKET:
                            bracketDown = 1;
                            break;
                    }
                    break;
                case SDL_KEYUP:
                    switch(event.key.keysym.sym){
                        case SDLK_LEFTBRACKET:
                            bracketDown = 0;
                            break;
                        case SDLK_RIGHTBRACKET:
                            bracketDown = 0;
                            break;
                    }
                    break;
                case SDL_WINDOWEVENT:
                    lastWindowBug = true;
                    lastWindowBugCtr = 1;
                    break;
                case SDL_QUIT:
                    quit = true;
                    break;
            }
            
            if(bracketDown != 0)
                cursorSize += 0.016 * bracketDown;
            
            if (!lastWindowBug && (leftMouseButtonDown || rightMouseButtonDown))
            {
                mouseX = event.motion.x;
                mouseY = event.motion.y;
                int centerX = mouseX * size / windowTarget.w;
                int centerY = mouseY * size / windowTarget.h;
                double set = leftMouseButtonDown ? 255.0 : 0;
                for(int i = centerX-cursorSize; i < centerX + cursorSize; i++)
                    for(int j = centerY-cursorSize; j < centerY + cursorSize; j++)
                        if((i-centerX)*(i-centerX) + (j-centerY)*(j-centerY) <= cursorSize*cursorSize)
                            setPoint(i,j,set, true);
            }
            
            if(lastWindowBugCtr == 1)
                lastWindowBugCtr = 2;
            if(lastWindowBugCtr == 2)
            {
                lastWindowBug = false;
                lastWindowBugCtr = 0;
            }
        }
        
        
        SDL_RenderClear(renderer);  
        SDL_RenderCopy(renderer, texture, &pixelTarget, &windowTarget);
        
        if(dispFont != NULL)
        {
            int centerX = mouseX * size / windowTarget.w;
            int centerY = mouseY * size / windowTarget.h;
            SDL_Color White = {255, 255, 255, 255};
            char dispTxt[100];
            snprintf(dispTxt, 100, "(%d,%d) = %.2f",centerX,centerY,data[newi][centerY*size+centerX]);
            SDL_Surface* surfaceMessage = TTF_RenderText_Blended(dispFont, dispTxt, White);
            SDL_Texture* Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);
            int x=4;
            int y=0;
            SDL_Rect Message_rect = {x, y, surfaceMessage->w, surfaceMessage->h};
            SDL_RenderCopy(renderer, Message, NULL, &Message_rect); 
            SDL_DestroyTexture(Message);
            SDL_FreeSurface(surfaceMessage);
        }
        
        SDL_RenderPresent(renderer);
        
        timeA += 0.016;
        SDL_Delay(16);
        newi = 1-newi;
        oldi = 1-oldi;
    }

    free(pixels);
    free(data[0]);
    free(data[1]);
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

void setup1()
{
    clearMesh();
    animate = 0;
    
    for(int i = 0; i < size; i++)
    {
        setPoint(0,size-i/2-1,255.0, true);
        setPoint(size-1,size-i/2-1,255.0, true);
        setPoint(i,size-1,255.0, true);
    }
}

void setup2()
{
    clearMesh();
    animate = 0;
    
    for(int i=1; i<size-1; i++)
        for(int j = size/2-5; j < size/2+5; j++)
            {
                setPoint(i,j,255.0, true);
                setPoint(j,i,255.0, true);
            }
}

void setup3()
{
    clearMesh();
    animate = 0;
    
    for(int i=1; i<size-1; i++)
        for(int j=1; j<size-1; j++)
            setPoint(i,j,255-255*sqrt(2)/size*sqrt((i-size/2)*(i-size/2) + (j-size/2)*(j-size/2)), true);
}

void setup4()
{
    clearMesh();
    animate = 0;
    
    for(int i=1; i<size-1; i++)
        for(int j=1; j<size-1; j++)
            setPoint(i,j,255.0 / 2 * abs(sin(0.2*60/size*i) + sin(0.2*60/size*j)), true);
}

void setup5()
{
    clearMesh();
    animate = 1;
    timeA = 0;
    
    for(int i = 1; i < size-1; i++)
        setPoint(size/2,i,255.0, true);
}

void setup6()
{
    clearMesh();
    animate = 2;
    timeA = 0;
    animData = 255.0;
}

void setup7()
{
    clearMesh();
    animate = 3;
    timeA = 0;
}

void animateGrid()
{
    double period;
    switch(animate)
    {
        case 1:
            period = 6;
            for(int i = 1; i < size-1; i++)
                setPoint((int)(size/2 + (size-2)/2.0*sin(2*M_PI/period*timeA)),i,255.0, true);
            if(timeA > period) timeA -= period;
            break;
        case 2:
            period = 7;
            anim6Helper(period, timeA, animData);
            if(timeA > period)
            {
                timeA=0;
                animData = 255.0-animData;
            }
            break;
        case 3:
            period = 7;
            int numDivisions = 8;
            for(int i=0; i<numDivisions; i++)
                anim6Helper(period, timeA - i*period/numDivisions,i%2==0 ? 255.0 : 0);
            if(timeA > period) timeA = 0;
            break;
    }
}

void anim6Helper(double period, double t, double val)
{
    while(t<0) t += period;
    if(t <= period/4)
        setPoint((int)((size-1)*4*t/period), 0, val, true);
    else if (t <= period / 2)
        setPoint(size-1,(int)((size-1)*4*(t-period/4)/period),val, true);
    else if (t <= 3 * period / 4)
        setPoint(size-1-(int)((size-1)*4*(t-period/2)/period),size-1,val, true);
    else if (t <= period)
        setPoint(0,size-1-(int)((size-1)*4*(t-3*period/4)/period),val, true);
}

void clearMesh()
{
    memset(pixels, 0, sizeof(Uint32) * size * size);
    memset(data[0], 0, sizeof(double) * size * size);
    memset(data[1], 0, sizeof(double) * size * size);
}

Uint32 RGBA2INT(int R, int G, int B, int A)
{
    return SDL_MapRGBA(SDL_AllocFormat(SDL_PIXELFORMAT_RGBA8888), R, G, B, A);
}

void setPoint(int x, int y, double val, bool both)
{
    if(x < 0 || y < 0 || x >= size || y >= size) return;
    
    if(val > 255) val = 255;
    if(val < 0) val = 0;

    pixels[y * size + x] = RGBA2INT(val,0,0,255);
    data[newi][y * size + x] = val;
    if(both)
        data[oldi][y * size + x] = val;
}
